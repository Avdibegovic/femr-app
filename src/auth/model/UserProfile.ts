export interface IUserProfile {
  userId: number;
  username: string;
}
