import HttpService, { IHttpService } from "../../base/service/HttpService";

export interface IAuthService {
  login(username: string, password: string): Promise<void>;
}

const AuthService = (): IAuthService => {
  const _http: IHttpService = HttpService();
  const _loginPath: string = "/login";
  return {
    async login(username: string, password: string) {
      await _http.post(
        `${_loginPath}?username=${username}&password=${password}`
      );
    },
  };
};

export default AuthService;
