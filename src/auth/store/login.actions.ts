import { createAction } from "@reduxjs/toolkit";
import { IUserProfile } from "../model/UserProfile";

export const login = createAction("[Login]");
export const loginSuccess = createAction(
  "[Login] success",
  (user: IUserProfile) => ({
    payload: user,
  })
);
export const loginFail = createAction("[Login] fail", (error: any) => ({
  payload: error,
}));
