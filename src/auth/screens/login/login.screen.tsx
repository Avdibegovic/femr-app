import React, { useEffect, useState } from "react";
import { Icon, Image, Input } from "@rneui/themed";
import { Text, View, SafeAreaView, ScrollView } from "react-native";
import { loginStyle } from "./login.style";
import { Formik } from "formik";
import { Button } from "@rneui/base";
import { loginForm as loginFormSchema } from "./login.form";
import { bindActionCreators } from "@reduxjs/toolkit";
import { connect } from "react-redux";
import { LoadingState } from "../../../base/store/loading/loadingState";
import { LoginState } from "../../store/LoginState";
import { AppState } from "../../../base/store/initial/AppState";
import { login, loginFail, loginSuccess } from "../../store/login.actions";
import { hide, show } from "../../../base/store/loading/loading.actions";
import AuthService from "../../service/AuthService";
import { showMessage } from "react-native-flash-message";

interface LoginScreenProps {
  navigation: any;

  loadingState: LoadingState;
  loginState: LoginState;

  login: Function;
  hideLoading: Function;
  showLoading: Function;
  loginSuccess: Function;
  loginFail: Function;
}

const {
  container,
  logoContainer,
  logoImage,
  loginContainer,
  loginForm,
  loginFormTitle,
  inputField,
  usernameInputIcon,
  passwordInputIcon,
  loginButtonStyle,
  loginButtonTitleStyle,
  loginButtonContainerStyle,
  errorMessage,
} = loginStyle;

const LoginScreen = (props: LoginScreenProps) => {
  const {
    navigation,
    login,
    loginState,
    showLoading,
    hideLoading,
    loginSuccess,
    loginFail,
  } = props;

  const [userLogin, setUserLogin] = useState({ username: "", password: "" });

  useEffect(() => {
    if (loginState.isLoggingIn) {
      showLoading();

      AuthService()
        .login(userLogin.username, userLogin.password)
        .then((user) => loginSuccess(user))
        .catch((error) => {
          loginFail(error);
          showMessage({ message: error.message, type: "danger" });
        });
    } else {
      hideLoading();
    }
  }, [loginState.isLoggingIn]);

  useEffect(() => {
    if (loginState.isLoggedIn) {
      hideLoading();
      navigation.navigate("PatientSearch");
    }
  }, [loginState.isLoggedIn]);

  const onLogin = (userLogin: { username: string; password: string }) => {
    setUserLogin(userLogin);
    login();
  };

  return (
    <SafeAreaView>
      <ScrollView contentContainerStyle={container}>
        <View style={logoContainer}>
          <Image
            source={require("./../../../../assets/images/medox_logo_transparent.png")}
            style={logoImage}
            resizeMode="center"
          />
        </View>
        <View style={loginContainer}>
          <View style={loginForm}>
            <Text style={loginFormTitle}>Prijava</Text>
            <Formik
              initialValues={{ username: "", password: "" }}
              onSubmit={onLogin}
              validationSchema={loginFormSchema}
            >
              {({
                handleSubmit,
                handleChange,
                errors,
                touched,
                setFieldTouched,
              }) => (
                <>
                  <Input
                    leftIcon={
                      <Icon
                        name="user"
                        type="simple-line-icon"
                        style={{ marginLeft: usernameInputIcon.marginLeft }}
                        color={usernameInputIcon.color}
                        size={25}
                      />
                    }
                    placeholder="Korisničko ime"
                    autoCapitalize="none"
                    autoCorrect={false}
                    keyboardType="email-address"
                    style={inputField}
                    returnKeyType="next"
                    onChangeText={handleChange("username")}
                    onFocus={() => setFieldTouched("username")}
                  />
                  <Text style={errorMessage}>{errors.username}</Text>
                  <Input
                    leftIcon={
                      <Icon
                        name="lock"
                        type="simple-line-icon"
                        style={{ marginLeft: passwordInputIcon.marginLeft }}
                        color={passwordInputIcon.color}
                        size={25}
                      />
                    }
                    placeholder="Lozinka"
                    autoCapitalize="none"
                    secureTextEntry={true}
                    autoCorrect={false}
                    keyboardType="default"
                    style={inputField}
                    returnKeyType="next"
                    onChangeText={handleChange("password")}
                    onFocus={() => setFieldTouched("password")}
                  />
                  <Text style={errorMessage}>{errors.password}</Text>
                  <Button
                    title="Prijavi se"
                    loading={false}
                    loadingProps={{ size: "small", color: "white" }}
                    buttonStyle={loginButtonStyle}
                    titleStyle={loginButtonTitleStyle}
                    containerStyle={loginButtonContainerStyle}
                    onPress={handleSubmit as any}
                  />
                </>
              )}
            </Formik>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const mapStateToProps = (store: AppState) => ({
  loadingState: store.loading,
  loginState: store.login,
});

const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      login: login,
      hideLoading: hide,
      showLoading: show,
      loginSuccess: loginSuccess,
      loginFail: loginFail,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
