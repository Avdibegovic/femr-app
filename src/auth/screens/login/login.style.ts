import { Dimensions, StyleSheet } from "react-native";

const SCREEN_HIGHT = Dimensions.get("window").height;

export const loginStyle = StyleSheet.create({
  container: {
    flexGrow: 1,
    height: 1.04 * SCREEN_HIGHT,
    color: "red",
  },
  logoContainer: {
    alignItems: "center",
    height: "40%",
    paddingTop: "30%",
    backgroundColor: "#f1f9f4",
  },
  logoImage: { width: 0.3 * SCREEN_HIGHT, height: 0.3 * SCREEN_HIGHT },
  loginContainer: {
    height: "60%",
    backgroundColor: "white",
  },
  loginForm: {
    backgroundColor: "#f3f7f3",
    width: "100%",
    height: "100%",
    alignItems: "center",
    paddingBottom: 30,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
  },
  loginFormTitle: {
    color: "#6ca59a",
    fontSize: 30,
    marginVertical: 10,
    fontWeight: "bold",
  },
  inputField: {
    borderWidth: 0,
  },
  usernameInputIcon: {
    color: "#ed1c24",
    marginLeft: 12,
  },
  passwordInputIcon: {
    color: "#ed1c24",
    marginLeft: 10,
  },
  loginButtonStyle: {
    backgroundColor: "#ed1c24",
    borderRadius: 10,
  },
  loginButtonTitleStyle: {
    fontWeight: "bold",
    fontSize: 23,
  },
  loginButtonContainerStyle: {
    marginHorizontal: 50,
    height: 50,
    width: "75%",
    marginVertical: 10,
  },
  errorMessage: {
    color: "red",
    marginTop: -20,
    marginBottom: 20,
  },
});
