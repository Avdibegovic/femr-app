import Model from "../../base/model/Model";

export interface IDrug {
  drugId: number;
  name: string;
  ndcNumber: string;
  onOrder: number;
  reorderPoint: number;
  maxLevel: number;
  lastNotify: Date;
  reactions: string;
  form: number;
  size: string;
  unit: number;
  route: number;
  substitute: number;
  relatedCode: string;
  cypFactor: number;
  active: boolean;
  allowCombining: boolean;
  allowMultiple: boolean;
  drugCode: string;
  consumable: boolean;
  dispensable: boolean;
}

const Drug = Model((model: IDrug) => {
  const _drug: IDrug = Object.assign({}, model);

  let drug = {
    get drugId() {
      return _drug.drugId;
    },
    get name() {
      return _drug.name;
    },
    get ndcNumber() {
      return _drug.ndcNumber;
    },
    get onOrder() {
      return _drug.onOrder;
    },
    get reorderPoint() {
      return _drug.reorderPoint;
    },
    get maxLevel() {
      return _drug.maxLevel;
    },
    get lastNotify() {
      return _drug.lastNotify;
    },
    get reactions() {
      return _drug.reactions;
    },
    get form() {
      return _drug.form;
    },
    get size() {
      return _drug.size;
    },
    get unit() {
      return _drug.unit;
    },
    get route() {
      return _drug.route;
    },
    get substitute() {
      return _drug.substitute;
    },
    get relatedCode() {
      return _drug.relatedCode;
    },
    get cypFactor() {
      return _drug.cypFactor;
    },
    get active() {
      return _drug.active;
    },
    get allowCombining() {
      return _drug.allowCombining;
    },
    get allowMultiple() {
      return _drug.allowMultiple;
    },
    get drugCode() {
      return _drug.drugCode;
    },
    get consumable() {
      return _drug.consumable;
    },
    get dispensable() {
      return _drug.dispensable;
    },
  };
  return drug;
});

export default Drug;
