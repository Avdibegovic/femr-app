import Drug from "../Drug";

export const DrugListMapper = (json: any[] = []) => {
  return json.map((item: any) => Drug(item));
};

export default DrugListMapper;
