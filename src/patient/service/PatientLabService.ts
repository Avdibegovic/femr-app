import HttpService, { IHttpService } from "../../base/service/HttpService";
import LabMeasurementListMapper from "../model/mappers/LabMeasurementListMapper";
import { IPatientLabMeasurement } from "../model/PatientLabMeasurement";

export interface IPatientLabService {
  getLabResultCodes(patientId: number): Promise<string[]>;
  getLabItemMeasurements(
    patientId: number,
    resultCode: string
  ): Promise<IPatientLabMeasurement[]>;
  getLabDates(patientId: number): Promise<number[]>;
  getLabDateMeasurements(
    patientId: number,
    collectionDate: Date
  ): Promise<IPatientLabMeasurement[]>;
}

const PatientLabService = (): IPatientLabService => {
  const _http: IHttpService = HttpService();
  const _baseUrl = "/api/patients";
  const _labPath = "/lab";
  const _codesPath = "/codes";
  const _collectionDatesPath = "/collection-dates";

  return {
    async getLabResultCodes(patientId: number) {
      const path = `${_baseUrl}/${patientId.toString()}${_labPath}${_codesPath}`;
      const response = await _http.get(path);
      const responseJSON = response.data;
      return responseJSON as string[];
    },

    async getLabItemMeasurements(patientId: number, resultCode: string) {
      const path = `${_baseUrl}/${patientId.toString()}${_labPath}${_codesPath}/${resultCode}`;
      const response = await _http.get(path);
      const responseJSON = response.data;
      return LabMeasurementListMapper(responseJSON);
    },

    async getLabDates(patientId: number) {
      const path = `${_baseUrl}/${patientId.toString()}${_labPath}${_collectionDatesPath}`;
      const response = await _http.get(path);
      const responseJSON = response.data;
      return responseJSON as number[];
    },

    async getLabDateMeasurements(patientId: number, collectionDate: Date) {
      const collectionDateTimestamp = collectionDate.getTime();
      const path = `${_baseUrl}/${patientId.toString()}${_labPath}${_collectionDatesPath}/${collectionDateTimestamp}`;
      const response = await _http.get(path);
      const responseJSON = response.data;
      return LabMeasurementListMapper(responseJSON);
    },
  };
};

export default PatientLabService;
