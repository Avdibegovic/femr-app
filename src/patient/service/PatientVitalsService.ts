import HttpService, { IHttpService } from "../../base/service/HttpService";
import { IBloodPressureOverviewItem } from "../model/BloodPressureOverviewItem";
import BloodPressureOverviewMapper from "../model/mappers/BloodPressureOverviewMapper";
import Pso2OverviewMapper from "../model/mappers/Pso2OverviewMapper";
import PulseOverviewMapper from "../model/mappers/PulseOverviewMapper";
import TemperatureOverviewMapper from "../model/mappers/TemperatureOverviewMapper";
import PatientVitals, { IPatientVitals } from "../model/PatientVitals";
import { IPatientVitalsMeasurement } from "../model/PatientVitalsMeasurement";
import { IPso2OverviewItem } from "../model/Pso2OverviewItem";
import { IPulseOverviewItem } from "../model/PulseOverviewItme";
import { ITemperatureOverviewItem } from "../model/TemperatureOverviewItem";

export interface IPatientVitalsService {
  getLatestPatientVitals(patientId: number): Promise<IPatientVitals>;
  getBloodPressureOverview(
    patientId: number
  ): Promise<IBloodPressureOverviewItem[]>;
  getPulseOverview(patientId: number): Promise<IPulseOverviewItem[]>;
  getTemperatureOverview(
    patientId: number
  ): Promise<ITemperatureOverviewItem[]>;
  getPso2Overview(patientId: number): Promise<IPso2OverviewItem[]>;
  insertPatientVitals(
    patientId: number,
    patientVitalsMeasurement: IPatientVitalsMeasurement
  ): Promise<void>;
}

const PatientVitalsService = (): IPatientVitalsService => {
  const _http: IHttpService = HttpService();
  const _baseUrl = "/api/patients";
  const _vitalsPath = "/vitals";
  const _latestPath = "/latest";
  const _bloodPressurePath = "/blood-pressure";
  const _pulsePath = "/pulse";
  const _temperaturePath = "/temperature";
  const _pso2Path = "/pso2";
  const _overviewPath = "/overview";

  return {
    async getLatestPatientVitals(patientId: number) {
      const path = `${_baseUrl}/${patientId.toString()}${_vitalsPath}${_latestPath}`;
      const response = await _http.get(path);
      const responseJSON = response.data;
      return PatientVitals(responseJSON);
    },
    async getBloodPressureOverview(patientId: number) {
      const path = `${_baseUrl}/${patientId.toString()}${_vitalsPath}${_bloodPressurePath}${_overviewPath}`;
      const response = await _http.get(path);
      const responseJSON = response.data;
      return BloodPressureOverviewMapper(responseJSON);
    },
    async getPulseOverview(patientId: number) {
      const path = `${_baseUrl}/${patientId.toString()}${_vitalsPath}${_pulsePath}${_overviewPath}`;
      const response = await _http.get(path);
      const responseJSON = response.data;
      return PulseOverviewMapper(responseJSON);
    },
    async getTemperatureOverview(patientId: number) {
      const path = `${_baseUrl}/${patientId.toString()}${_vitalsPath}${_temperaturePath}${_overviewPath}`;
      const response = await _http.get(path);
      const responseJSON = response.data;
      return TemperatureOverviewMapper(responseJSON);
    },
    async getPso2Overview(patientId: number) {
      const path = `${_baseUrl}/${patientId.toString()}${_vitalsPath}${_pso2Path}${_overviewPath}`;
      const response = await _http.get(path);
      const responseJSON = response.data;
      return Pso2OverviewMapper(responseJSON);
    },
    async insertPatientVitals(
      patientId: number,
      patientVitalsMeasurement: IPatientVitalsMeasurement
    ) {
      const path = `${_baseUrl}/${patientId.toString()}${_vitalsPath}`;
      let measurement = patientVitalsMeasurement;
      await _http.post(path, measurement);
    },
  };
};

export default PatientVitalsService;
