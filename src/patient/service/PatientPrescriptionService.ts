import HttpService, { IHttpService } from "../../base/service/HttpService";
import patientPrescriptionListMapper from "../model/mappers/PatientPrescriptionListMapper";
import { IPatientPrescription } from "../model/PatientPrescription";

export interface IPatientPrescriptionService {
  getActivePrescriptions(patientId: number): Promise<IPatientPrescription[]>;
}

const PatientPrescriptionService = (): IPatientPrescriptionService => {
  const _http: IHttpService = HttpService();
  const _baseUrl = "/api/patients";
  const _prescriptionsPath = "/prescriptions";
  const _activePath = "/active";

  return {
    async getActivePrescriptions(patientId: number) {
      const path = `${_baseUrl}/${patientId}${_prescriptionsPath}${_activePath}`;
      const response = await _http.get(path);
      const responseJSON = response.data;
      return patientPrescriptionListMapper(responseJSON);
    },
  };
};

export default PatientPrescriptionService;
