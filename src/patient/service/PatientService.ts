import HttpService, { IHttpService } from "../../base/service/HttpService";
import Patient, { IPatient } from "../model/Patient";
import { IPatientSearchParams } from "../model/PatientSearchParams";

export interface IPatientService {
  getPatients(params: IPatientSearchParams): Promise<IPatient[]>;
  getPatient(id: number): Promise<IPatient>;
}

const PatientService = (): IPatientService => {
  const _http: IHttpService = HttpService();
  const _basePath = "/api/patients";

  const mapPatientsList = (json: any[]): IPatient[] => {
    return json.map((item: any) => {
      return Patient(item);
    });
  };

  return {
    async getPatients(params: IPatientSearchParams) {
      const response = await _http.get(_basePath, params);
      const responseJSON = response.data;
      return mapPatientsList(responseJSON);
    },

    async getPatient(id: number) {
      const response = await _http.get(`${_basePath}/${id.toString()}`);
      const responseJSON = response.data;
      return Patient(responseJSON);
    },
  };
};

export default PatientService;
