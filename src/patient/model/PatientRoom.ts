import Model from "../../base/model/Model";
import { IPatient } from "./Patient";

export interface IPatientRoom {
  roomNumber: string;
  patients: IPatient[];
}

const PatientRoom = Model((model: IPatientRoom): IPatientRoom => {
  const _patientRoom: IPatientRoom = Object.assign({}, model);

  let patientRoom = {
    get roomNumber() {
      return _patientRoom.roomNumber;
    },
    get patients() {
      return _patientRoom.patients;
    },
  };
  return patientRoom;
});

export default PatientRoom;
