import Model from "../../base/model/Model";

export interface IBloodPressureOverviewItem {
  date: Date;
  bpd: number;
  bps: number;
}

const BloodPressureOverviewItem = Model(
  (model: IBloodPressureOverviewItem): IBloodPressureOverviewItem => {
    const _bloodPressureOverviewItem: IBloodPressureOverviewItem =
      Object.assign({}, model);

    let bloodPressureOverviewItem = {
      get date() {
        return _bloodPressureOverviewItem.date;
      },
      get bpd() {
        return _bloodPressureOverviewItem.bpd;
      },
      get bps() {
        return _bloodPressureOverviewItem.bps;
      },
    };
    return bloodPressureOverviewItem;
  }
);

export default BloodPressureOverviewItem;
