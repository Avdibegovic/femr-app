import Model from "../../base/model/Model";

export interface IPatient {
  id: number;
  firstname: string;
  lastname: string;
  sex: string;
  dateOfBirth: Date;
  department: string;
  importantNotice: string;
  roomNumber: string;
}

const Patient = Model((model: IPatient): IPatient => {
  const _patient: IPatient = Object.assign({}, model);

  let patient = {
    get id() {
      return _patient.id;
    },
    get firstname() {
      return _patient.firstname;
    },
    get lastname() {
      return _patient.lastname;
    },
    get sex() {
      return _patient.sex;
    },
    get dateOfBirth() {
      return _patient.dateOfBirth;
    },
    get department() {
      return _patient.department;
    },
    get importantNotice() {
      return _patient.importantNotice;
    },
    get roomNumber() {
      return _patient.roomNumber;
    },
  };
  return patient;
});

export default Patient;
