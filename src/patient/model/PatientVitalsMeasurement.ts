import Model from "../../base/model/Model";

export interface IPatientVitalsMeasurement {
  date: Date;
  bps?: number;
  bpd?: number;
  temperature?: number;
  pulse?: number;
  pso2?: number;
  hydration?: number;
  bloodGlucose?: number;
  diuresis?: number;
}

const PatientVitalsMeasurement = Model(
  (model: IPatientVitalsMeasurement): IPatientVitalsMeasurement => {
    const _patientVitalsMeasurement: IPatientVitalsMeasurement = Object.assign(
      {},
      model
    );

    let patientVitalsMeasurement = {
      get date() {
        return _patientVitalsMeasurement.date;
      },
      set date(value) {
        _patientVitalsMeasurement.date = value;
      },
      get bps() {
        return _patientVitalsMeasurement.bps;
      },
      set bps(value) {
        _patientVitalsMeasurement.bps = value;
      },
      get bpd() {
        return _patientVitalsMeasurement.bpd;
      },
      set bpd(value) {
        _patientVitalsMeasurement.bpd = value;
      },
      get temperature() {
        return _patientVitalsMeasurement.temperature;
      },
      set temperature(value) {
        _patientVitalsMeasurement.temperature = value;
      },
      get pulse() {
        return _patientVitalsMeasurement.pulse;
      },
      set pulse(value) {
        _patientVitalsMeasurement.pulse = value;
      },
      get pso2() {
        return _patientVitalsMeasurement.pso2;
      },
      set pso2(value) {
        _patientVitalsMeasurement.pso2 = value;
      },
      get hydration() {
        return _patientVitalsMeasurement.hydration;
      },
      set hydration(value) {
        _patientVitalsMeasurement.hydration = value;
      },
      get bloodGlucose() {
        return _patientVitalsMeasurement.bloodGlucose;
      },
      set bloodGlucose(value) {
        _patientVitalsMeasurement.bloodGlucose = value;
      },
      get diuresis() {
        return _patientVitalsMeasurement.diuresis;
      },
      set diuresis(value) {
        _patientVitalsMeasurement.diuresis = value;
      },
    };
    return patientVitalsMeasurement;
  }
);

export default PatientVitalsMeasurement;
