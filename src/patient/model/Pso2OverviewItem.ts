import Model from "../../base/model/Model";

export interface IPso2OverviewItem {
  date: Date;
  pso2: number;
}

const Pso2OverviewItem = Model((model: IPso2OverviewItem) => {
  const _pso2OverviewItem: IPso2OverviewItem = Object.assign({}, model);

  let pso2OverviewItem = {
    get date() {
      return _pso2OverviewItem.date;
    },
    get pso2() {
      return _pso2OverviewItem.pso2;
    },
  };
  return pso2OverviewItem;
});

export default Pso2OverviewItem;
