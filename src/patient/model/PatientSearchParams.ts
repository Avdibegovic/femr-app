export interface IPatientSearchParams {
  startsWith?: string;
  facility?: string;
  status?: string;
}
