import TemperatureOverviewItem from "../TemperatureOverviewItem";

const TemperatureOverviewMapper = (json: any[] = []) => {
  return json.map((item: any) => TemperatureOverviewItem(item));
};

export default TemperatureOverviewMapper;
