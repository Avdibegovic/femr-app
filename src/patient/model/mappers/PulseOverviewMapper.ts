import PulseOverviewItem from "../PulseOverviewItme";

const PulseOverviewMapper = (json: any[] = []) => {
  return json.map((item: any) => PulseOverviewItem(item));
};

export default PulseOverviewMapper;
