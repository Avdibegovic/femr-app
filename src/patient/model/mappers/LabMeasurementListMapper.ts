import PatientLabMeasurement from "../PatientLabMeasurement";

const LabMeasurementListMapper = (json: any[] = []) => {
  return json.map((item: any) => PatientLabMeasurement(item));
};

export default LabMeasurementListMapper;
