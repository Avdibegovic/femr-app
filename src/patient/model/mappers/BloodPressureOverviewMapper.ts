import BloodPressureOverviewItem from "../BloodPressureOverviewItem";

const BloodPressureOverviewMapper = (json: any[] = []) => {
  return json.map((item: any) => BloodPressureOverviewItem(item));
};

export default BloodPressureOverviewMapper;
