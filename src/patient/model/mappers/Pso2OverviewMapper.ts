import Pso2OverviewItem from "../Pso2OverviewItem";

const Pso2OverviewMapper = (json: any[] = []) => {
  return json.map((item: any) => Pso2OverviewItem(item));
};

export default Pso2OverviewMapper;
