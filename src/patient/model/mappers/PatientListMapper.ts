import Patient, { IPatient } from "../Patient";

const PatientListMapper = (json: any[]): IPatient[] => {
  return json.map((item: any) => {
    return Patient(item);
  });
};

export default PatientListMapper;
