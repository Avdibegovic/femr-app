import PatientPrescription from "../PatientPrescription";

const patientPrescriptionListMapper = (json: any = []) => {
  return json.map((item: any) => PatientPrescription(item));
};

export default patientPrescriptionListMapper;
