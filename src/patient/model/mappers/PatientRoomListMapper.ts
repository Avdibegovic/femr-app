import { IPatient } from "../Patient";
import PatientRoom from "../PatientRoom";

const patientRoomListMapper = (patients: IPatient[]) => {
  const roomNumbers = patients
    .map((patient: IPatient) => patient.roomNumber)
    .filter(
      (value: string, index: number, self: string[]) =>
        self.indexOf(value) === index
    );

  return roomNumbers.map((roomNumber: string) => {
    const roomPatients = patients.filter(
      (patient: IPatient) => patient.roomNumber === roomNumber
    );
    return PatientRoom({ roomNumber, patients: roomPatients });
  });
};

export default patientRoomListMapper;
