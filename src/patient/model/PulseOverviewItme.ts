import Model from "../../base/model/Model";

export interface IPulseOverviewItem {
  date: Date;
  pulse: number;
}

const PulseOverviewItem = Model(
  (model: IPulseOverviewItem): IPulseOverviewItem => {
    const _pulseOverviewItem: IPulseOverviewItem = Object.assign({}, model);

    let pulseOverviewItem = {
      get date() {
        return _pulseOverviewItem.date;
      },
      get pulse() {
        return _pulseOverviewItem.pulse;
      },
    };
    return pulseOverviewItem;
  }
);

export default PulseOverviewItem;
