import Model from "../../base/model/Model";

export interface IPatientLabMeasurement {
  procedureResultId: number;
  result: string;
  resultText: string;
  resultCode: string;
  units: string;
  abnormal: string;
  range: string;
  dateCollected: Date;
  reviewStatus: string;
  encounterId: number;
}

const PatientLabMeasurement = Model(
  (model: IPatientLabMeasurement): IPatientLabMeasurement => {
    const _patientLabMeasurement: IPatientLabMeasurement = Object.assign(
      {},
      model
    );

    let patientLabMeasurement = {
      get procedureResultId() {
        return _patientLabMeasurement.procedureResultId;
      },
      get result() {
        return _patientLabMeasurement.result;
      },
      get resultText() {
        return _patientLabMeasurement.resultText;
      },
      get resultCode() {
        return _patientLabMeasurement.resultCode;
      },
      get units() {
        return _patientLabMeasurement.units;
      },
      get abnormal() {
        return _patientLabMeasurement.abnormal;
      },
      get range() {
        return _patientLabMeasurement.range;
      },
      get dateCollected() {
        return _patientLabMeasurement.dateCollected;
      },
      get reviewStatus() {
        return _patientLabMeasurement.reviewStatus;
      },
      get encounterId() {
        return _patientLabMeasurement.encounterId;
      },
    };
    return patientLabMeasurement;
  }
);

export default PatientLabMeasurement;
