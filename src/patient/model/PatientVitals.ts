import Model from "../../base/model/Model";

export interface IPatientVitals {
  bloodPressureDate: Date;
  bps: number;
  bpd: number;
  temperatureDate: Date;
  temperature: number;
  pulseDate: Date;
  pulse: number;
  pso2Date: Date;
  pso2: number;
  hydrationDate: Date;
  hydration: number;
  last24hHydration: number;
  bloodGlucoseDate: Date;
  bloodGlucose: number;
  diuresisDate: Date;
  diuresis: number;
}

const PatientVitals = Model((model: IPatientVitals): IPatientVitals => {
  const _patientVitals: IPatientVitals = Object.assign({}, model);

  let patientVitals = {
    get bloodPressureDate() {
      return _patientVitals.bloodPressureDate;
    },
    set bloodPressureDate(value) {
      _patientVitals.bloodPressureDate = value;
    },
    get bps() {
      return _patientVitals.bps;
    },
    set bps(value) {
      _patientVitals.bps = value;
    },
    get bpd() {
      return _patientVitals.bpd;
    },
    set bpd(value) {
      _patientVitals.bpd = value;
    },
    get temperatureDate() {
      return _patientVitals.temperatureDate;
    },
    set temperatureDate(value) {
      _patientVitals.temperatureDate = value;
    },
    get temperature() {
      return _patientVitals.temperature;
    },
    set temperature(value) {
      _patientVitals.temperature = value;
    },
    get pulseDate() {
      return _patientVitals.pulseDate;
    },
    set pulseDate(value) {
      _patientVitals.pulseDate = value;
    },
    get pulse() {
      return _patientVitals.pulse;
    },
    set pulse(value) {
      _patientVitals.pulse = value;
    },
    get pso2Date() {
      return _patientVitals.pso2Date;
    },
    set pso2Date(value) {
      _patientVitals.pso2Date = value;
    },
    get pso2() {
      return _patientVitals.pso2;
    },
    set pso2(value) {
      _patientVitals.pso2 = value;
    },
    get hydrationDate() {
      return _patientVitals.hydrationDate;
    },
    set hydrationDate(value) {
      _patientVitals.hydrationDate = value;
    },
    get hydration() {
      return _patientVitals.hydration;
    },
    set hydration(value) {
      _patientVitals.hydration = value;
    },
    get last24hHydration() {
      return _patientVitals.last24hHydration;
    },
    set last24hHydration(value) {
      _patientVitals.last24hHydration = value;
    },
    get bloodGlucoseDate() {
      return _patientVitals.bloodGlucoseDate;
    },
    set bloodGlucoseDate(value) {
      _patientVitals.bloodGlucoseDate = value;
    },
    get bloodGlucose() {
      return _patientVitals.bloodGlucose;
    },
    set bloodGlucose(value) {
      _patientVitals.bloodGlucose = value;
    },
    get diuresisDate() {
      return _patientVitals.diuresisDate;
    },
    set diuresisDate(value) {
      _patientVitals.diuresisDate;
    },
    get diuresis() {
      return _patientVitals.diuresis;
    },
    set diuresis(value) {
      _patientVitals.diuresis = value;
    },
  };
  return patientVitals;
});

export default PatientVitals;
