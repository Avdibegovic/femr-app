import Model from "../../base/model/Model";

export interface ITemperatureOverviewItem {
  date: Date;
  temperature: number;
}

const TemperatureOverviewItem = Model(
  (model: ITemperatureOverviewItem): ITemperatureOverviewItem => {
    const _temperatureOverviewItem: ITemperatureOverviewItem = Object.assign(
      {},
      model
    );

    let temperatureOverviewItem = {
      get date() {
        return _temperatureOverviewItem.date;
      },
      get temperature() {
        return _temperatureOverviewItem.temperature;
      },
    };
    return temperatureOverviewItem;
  }
);

export default TemperatureOverviewItem;
