import Model from "../../base/model/Model";

export interface IPatientPrescription {
  id: number;
  patientId: number;
  drug: string;
  drugId: number;
  form: number;
  drugFormName: string;
  dosage: string;
  quantity: string;
  size: string;
  unit: number;
  drugUnitName: string;
  route: number;
  drugRouteName: string;
  interval: number;
  drugIntervalName: string;
  note: string;
}

const PatientPrescription = Model(
  (model: IPatientPrescription): IPatientPrescription => {
    const _patientPrescription: IPatientPrescription = Object.assign({}, model);

    let patientPrescription = {
      get id() {
        return _patientPrescription.id;
      },
      get patientId() {
        return _patientPrescription.patientId;
      },
      get drug() {
        return _patientPrescription.drug;
      },
      get drugId() {
        return _patientPrescription.drugId;
      },
      get form() {
        return _patientPrescription.form;
      },
      get drugFormName() {
        return _patientPrescription.drugFormName;
      },
      get dosage() {
        return _patientPrescription.dosage;
      },
      get quantity() {
        return _patientPrescription.quantity;
      },
      get size() {
        return _patientPrescription.size;
      },
      get unit() {
        return _patientPrescription.unit;
      },
      get drugUnitName() {
        return _patientPrescription.drugUnitName;
      },
      get route() {
        return _patientPrescription.route;
      },
      get drugRouteName() {
        return _patientPrescription.drugRouteName;
      },
      get interval() {
        return _patientPrescription.interval;
      },
      get drugIntervalName() {
        return _patientPrescription.drugIntervalName;
      },
      get note() {
        return _patientPrescription.note;
      },
    };
    return patientPrescription;
  }
);

export default PatientPrescription;
