import { IPatient } from "../../model/Patient";
import { IPatientRoom } from "../../model/PatientRoom";

export interface PatientSearchState {
  error: any;
  isSearching: boolean;
  patientRooms?: IPatientRoom[];
  patientsLoading: boolean;
  recentPatientsLoading: boolean;
  recentPatients?: IPatient[];
}
