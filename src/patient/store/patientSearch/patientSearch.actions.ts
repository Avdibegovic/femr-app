import { createAction } from "@reduxjs/toolkit";
import { IPatient } from "../../model/Patient";
import { IPatientRoom } from "../../model/PatientRoom";

export const searchPatients = createAction("[Search Patients]");
export const searchPatientsSuccess = createAction(
  "[Search Patients] success",
  (patientRooms: IPatientRoom[]) => ({
    payload: patientRooms,
  })
);
export const searchPatientsFail = createAction(
  "[Search Patients] fail",
  (error: any) => ({ payload: error })
);
export const resetPatients = createAction("[Reset Patients]");
export const getRecentPatients = createAction("[Get Recent Patients]");
export const getRecentPatientsSuccess = createAction(
  "[Get Recent Patients] success",
  (patients: IPatient[] | null) => ({ payload: patients })
);
export const getRecentPatientsFail = createAction(
  "[Get Recent Patients] fail",
  (error: any) => ({ payload: error })
);
export const activateSearchLoader = createAction("[Activate Search Loader]");
export const deactivateSearchLoader = createAction(
  "[Deactivate Search Loader]"
);
