import { createReducer } from "@reduxjs/toolkit";
import { AppInitialState } from "../../../base/store/initial/AppInitialState";
import { IPatient } from "../../model/Patient";
import {
  activateSearchLoader,
  deactivateSearchLoader,
  getRecentPatients,
  getRecentPatientsFail,
  getRecentPatientsSuccess,
  resetPatients,
  searchPatients,
  searchPatientsFail,
  searchPatientsSuccess,
} from "./patientSearch.actions";
import { PatientSearchState } from "./PatientSearchState";

const initialState: PatientSearchState = AppInitialState.patientSearch;

export const patientSearchReducer = createReducer(initialState, (builder) => {
  builder.addCase(searchPatients, (currentState) => {
    return {
      ...currentState,
      error: null,
      isSearching: true,
      patients: [] as IPatient[],
    };
  });
  builder.addCase(searchPatientsSuccess, (currentState, action) => {
    return {
      ...currentState,
      error: null,
      isSearching: false,
      patientRooms: action.payload,
    };
  });
  builder.addCase(searchPatientsFail, (currentState, action) => {
    return {
      ...currentState,
      error: action.payload,
      isSearching: false,
      patients: [] as IPatient[],
    };
  });
  builder.addCase(resetPatients, (currentState) => {
    return {
      ...currentState,
      patients: undefined,
      isSearching: false,
    };
  });
  builder.addCase(getRecentPatients, (currentState, action) => {
    return {
      ...currentState,
      recentPatientsLoading: true,
      error: null,
    };
  });
  builder.addCase(getRecentPatientsSuccess, (currentState, action) => {
    return {
      ...currentState,
      recentPatients: action.payload || undefined,
      recentPatientsLoading: false,
      error: null,
    };
  });
  builder.addCase(getRecentPatientsFail, (currentState, action) => {
    return {
      ...currentState,
      error: action.payload,
      recentPatients: undefined,
      recentPatientsLoading: false,
    };
  });
  builder.addCase(activateSearchLoader, (currentState) => {
    return {
      ...currentState,
      patientsLoading: true,
    };
  });
  builder.addCase(deactivateSearchLoader, (currentState) => {
    return {
      ...currentState,
      patientsLoading: false,
    };
  });
});
