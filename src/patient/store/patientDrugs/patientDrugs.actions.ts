import { createAction } from "@reduxjs/toolkit";
import { IDrug } from "../../../drug/model/Drug";
import { IPatientDrugSale } from "../../model/PatientDrugSale";

export const getPatientDrugs = createAction("[Get Patient Drugs]");
export const getPatientDrugsSuccess = createAction(
  "[Get Patient Drugs] Success",
  (patientDrugs: IDrug[]) => ({ payload: patientDrugs })
);
export const getPatientDrugsFail = createAction(
  "[Get Patient Drugs] Fail",
  (error: any) => ({ payload: error })
);
export const getDrugSaleDates = createAction("[Get Drug-Sale Dates]");
export const getDrugSaleDatesSuccess = createAction(
  "[Get Drug-Sale Dates] Success",
  (drugSaleDate: number[]) => ({ payload: drugSaleDate })
);
export const getDrugSaleDatesFail = createAction(
  "[Get Drug-Sale Dates] Fail",
  (error: any) => ({ payload: error })
);
export const getPatientDrugSales = createAction("[Get Patient Drug Sales]");
export const getPatientDrugSalesSuccess = createAction(
  "[Get Patient Drug Sales] Success",
  (patientDrugSales: IPatientDrugSale[]) => ({ payload: patientDrugSales })
);
export const getPatientDrugSalesFail = createAction(
  "[Get Patient Drug Sales] Fail",
  (error: any) => ({ payload: error })
);
