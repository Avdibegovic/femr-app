import { createReducer } from "@reduxjs/toolkit";
import { AppInitialState } from "../../../base/store/initial/AppInitialState";
import { IDrug } from "../../../drug/model/Drug";
import {
  getDrugSaleDates,
  getDrugSaleDatesFail,
  getDrugSaleDatesSuccess,
  getPatientDrugs,
  getPatientDrugSales,
  getPatientDrugSalesFail,
  getPatientDrugSalesSuccess,
  getPatientDrugsFail,
  getPatientDrugsSuccess,
} from "./patientDrugs.actions";
import { PatientDrugsState } from "./PatientDrugsState";

const initialState: PatientDrugsState = AppInitialState.patientDrugs;

export const patientDrugsReducer = createReducer(initialState, (builder) => {
  builder.addCase(getPatientDrugs, (currentState) => {
    return {
      ...currentState,
      drugsLoading: true,
    };
  });
  builder.addCase(getPatientDrugsSuccess, (currentState, builder) => {
    return {
      ...currentState,
      drugsLoading: false,
      patientDrugs: builder.payload,
    };
  });
  builder.addCase(getPatientDrugsFail, (currentState, builder) => {
    return {
      ...currentState,
      drugsLoading: false,
      patientDrugs: [] as IDrug[],
      error: builder.payload,
    };
  });
  builder.addCase(getDrugSaleDates, (currentState) => {
    return {
      ...currentState,
      drugsLoading: true,
    };
  });
  builder.addCase(getDrugSaleDatesSuccess, (currentState, builder) => {
    return {
      ...currentState,
      drugsLoading: false,
      drugSaleDates: builder.payload,
    };
  });
  builder.addCase(getDrugSaleDatesFail, (currentState, builder) => {
    return {
      ...currentState,
      drugsLoading: false,
      drugSaleDates: [] as number[],
      error: builder.payload,
    };
  });
  builder.addCase(getPatientDrugSales, (currentState) => {
    return {
      ...currentState,
    };
  });
  builder.addCase(getPatientDrugSalesSuccess, (currentState, builder) => {
    return {
      ...currentState,
      patientDrugSales: builder.payload,
    };
  });
  builder.addCase(getPatientDrugSalesFail, (currentState, builder) => {
    return {
      ...currentState,
      patientDrugSales: undefined,
      error: builder.payload,
    };
  });
});
