import { IDrug } from "../../../drug/model/Drug";
import { IPatientDrugSale } from "../../model/PatientDrugSale";

export interface PatientDrugsState {
  drugsLoading: boolean;
  patientDrugs: IDrug[];
  drugSaleDates: number[];
  patientDrugSales?: IPatientDrugSale[];
}
