import { createReducer } from "@reduxjs/toolkit";
import { AppInitialState } from "../../../base/store/initial/AppInitialState";
import { BloodPressureOverviewState } from "./BloodPressureOverviewState";
import {
  getBloodPressureOverview,
  getBloodPressureOverviewFail,
  getBloodPressureOverviewSuccess,
} from "./bloodPressureOverview.actions";

const initialState: BloodPressureOverviewState =
  AppInitialState.bloodPressureOverview;

export const bloodPressureOverviewReducer = createReducer(
  initialState,
  (builder) => {
    builder.addCase(getBloodPressureOverview, (currentState) => {
      return {
        ...currentState,
        error: null,
        bloodPressureOverviewLoading: true,
      };
    });
    builder.addCase(getBloodPressureOverviewSuccess, (currentState, action) => {
      return {
        ...currentState,
        bloodPressureOverviewLoading: false,
        bloodPressureOverview: action.payload,
      };
    });
    builder.addCase(getBloodPressureOverviewFail, (currentSate, action) => {
      return {
        ...currentSate,
        bloodPressureOverviewLoading: false,
        error: action.payload,
      };
    });
  }
);
