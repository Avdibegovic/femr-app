import { IBloodPressureOverviewItem } from "../../model/BloodPressureOverviewItem";

export interface BloodPressureOverviewState {
  bloodPressureOverview: IBloodPressureOverviewItem[];
  bloodPressureOverviewLoading: boolean;
}
