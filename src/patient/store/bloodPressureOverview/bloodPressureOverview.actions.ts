import { createAction } from "@reduxjs/toolkit";
import { IBloodPressureOverviewItem } from "../../model/BloodPressureOverviewItem";

export const getBloodPressureOverview = createAction(
  "[Blood Pressure Overview]"
);
export const getBloodPressureOverviewSuccess = createAction(
  "[Blood Pressure Overview] Success",
  (bloodPressureOverview: IBloodPressureOverviewItem[]) => ({
    payload: bloodPressureOverview,
  })
);
export const getBloodPressureOverviewFail = createAction(
  "[Blood Pressure Overview] Fail",
  (error: any) => ({ payload: error })
);
