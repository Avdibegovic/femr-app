import { createReducer } from "@reduxjs/toolkit";
import { AppInitialState } from "../../../base/store/initial/AppInitialState";
import {
  getPatientPrescriptions,
  getPatientPrescriptionsFail,
  getPatientPrescriptionsSuccess,
} from "./patientPrescriptions.actions";
import { PatientPrescriptionsState } from "./PatientPrescriptionsState";

const initialState: PatientPrescriptionsState =
  AppInitialState.patientPrescriptions;

export const patientPrescriptionsReducer = createReducer(
  initialState,
  (builder) => {
    builder.addCase(getPatientPrescriptions, (currentState) => {
      return {
        ...currentState,
        patientPrescriptionsLoading: true,
      };
    });
    builder.addCase(getPatientPrescriptionsSuccess, (currentState, action) => {
      return {
        ...currentState,
        patientPrescriptions: action.payload,
        patientPrescriptionsLoading: false,
      };
    });
    builder.addCase(getPatientPrescriptionsFail, (currentState, action) => {
      return {
        ...currentState,
        patientPrescriptionsLoading: false,
        patientPrescriptions: action.payload,
      };
    });
  }
);
