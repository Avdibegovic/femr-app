import { createAction } from "@reduxjs/toolkit";
import { IPrescription } from "../../../prescription/model/Prescription";

export const getPatientPrescriptions = createAction(
  "[Get Patient Prescriptions]"
);

export const getPatientPrescriptionsSuccess = createAction(
  "[Get Patient Prescriptions] Success",
  (patientPrescriptions: IPrescription[]) => ({ payload: patientPrescriptions })
);

export const getPatientPrescriptionsFail = createAction(
  "[Get Patient Prescriptions]  Fail",
  (error: any) => ({ payload: error })
);
