import { IPrescription } from "../../../prescription/model/Prescription";

export interface PatientPrescriptionsState {
  patientPrescriptionsLoading: boolean;
  patientPrescriptions: IPrescription[];
}
