import { createAction } from "@reduxjs/toolkit";
import { IPatientLabMeasurement } from "../../model/PatientLabMeasurement";

export const getLabResultCodes = createAction("[Get Lab Result Codes]");
export const getLabResultCodesSuccess = createAction(
  "[Get Lab Result Codes]  Success",
  (resultCodes: string[]) => ({
    payload: resultCodes,
  })
);
export const getLabResultCodesFail = createAction(
  "[Get Lab Result Code]  Fail",
  (error: any) => ({
    payload: error,
  })
);
export const getLabMeasurements = createAction("[Get Lab Measurements]");
export const getLabMeasurementsSuccess = createAction(
  "[Get Lab Measurements] Success",
  (labMeasurements: IPatientLabMeasurement[]) => ({
    payload: labMeasurements,
  })
);
export const getLabMeasurementsFail = createAction(
  "[Get Lab Measurements] Fail",
  (error: any) => ({
    payload: error,
  })
);
export const getLabDates = createAction("[Get Lab Dates]");
export const getLabDatesSuccess = createAction(
  "[Get Lab Dates] Success",
  (labDates: number[]) => ({
    payload: labDates,
  })
);
export const getLabDatesFail = createAction(
  "[Get Lab Dates] Fail",
  (error: any) => ({
    payload: error,
  })
);
