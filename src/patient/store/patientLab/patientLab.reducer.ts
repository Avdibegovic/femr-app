import { createReducer } from "@reduxjs/toolkit";
import { AppInitialState } from "../../../base/store/initial/AppInitialState";
import {
  getLabDates,
  getLabDatesFail,
  getLabDatesSuccess,
  getLabMeasurements,
  getLabMeasurementsFail,
  getLabMeasurementsSuccess,
  getLabResultCodes,
  getLabResultCodesFail,
  getLabResultCodesSuccess,
} from "./patientLab.actions";
import { PatientLabState } from "./PatientLabState";

const initialState: PatientLabState = AppInitialState.patientLab;

export const patientLabRedcuer = createReducer(initialState, (builder) => {
  builder.addCase(getLabResultCodes, (currentState) => {
    return {
      ...currentState,
      labLoading: true,
    };
  });
  builder.addCase(getLabResultCodesSuccess, (currentState, builder) => {
    return {
      ...currentState,
      labLoading: false,
      resultCodes: builder.payload,
    };
  });
  builder.addCase(getLabResultCodesFail, (currentState, builder) => {
    return {
      ...currentState,
      labLoading: false,
      resultCodes: [] as string[],
      error: builder.payload,
    };
  });
  builder.addCase(getLabMeasurements, (currentState) => {
    return {
      ...currentState,
    };
  });
  builder.addCase(getLabMeasurementsSuccess, (currentState, builder) => {
    return {
      ...currentState,
      labMeasurements: builder.payload,
    };
  });
  builder.addCase(getLabMeasurementsFail, (currentState, builder) => {
    return {
      ...currentState,
      labMeasurements: undefined,
      error: builder.payload,
    };
  });
  builder.addCase(getLabDates, (currentState) => {
    return {
      ...currentState,
      labLoading: true,
    };
  });
  builder.addCase(getLabDatesSuccess, (currentState, builder) => {
    return {
      ...currentState,
      labLoading: false,
      labDates: builder.payload,
    };
  });
  builder.addCase(getLabDatesFail, (currentState, builder) => {
    return {
      ...currentState,
      labLoading: false,
      labMeasurements: undefined,
      error: builder.payload,
    };
  });
});
