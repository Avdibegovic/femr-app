import { IPatientLabMeasurement } from "../../model/PatientLabMeasurement";

export interface PatientLabState {
  resultCodes: string[];
  labMeasurements?: IPatientLabMeasurement[];
  labLoading: boolean;
  labDates: number[];
}
