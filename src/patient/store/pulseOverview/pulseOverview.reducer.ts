import { createReducer } from "@reduxjs/toolkit";
import { AppInitialState } from "../../../base/store/initial/AppInitialState";
import {
  getPulseOverview,
  getPulseOverviewFail,
  getPulseOverviewSuccess,
} from "./pulseOverview.actions";
import { PulseOverviewState } from "./PulseOverviewState";

const initialState: PulseOverviewState = AppInitialState.pulseOverview;

export const pulseOverviewReducer = createReducer(initialState, (builder) => {
  builder.addCase(getPulseOverview, (currentState) => {
    return {
      ...currentState,
      error: null,
      pulseOverviewLoading: true,
    };
  });
  builder.addCase(getPulseOverviewSuccess, (currentState, action) => {
    return {
      ...currentState,
      pulseOverview: action.payload,
      pulseOverviewLoading: false,
    };
  });
  builder.addCase(getPulseOverviewFail, (currentState, action) => {
    return {
      ...currentState,
      pulseOverviewLoading: false,
      error: action.payload,
    };
  });
});
