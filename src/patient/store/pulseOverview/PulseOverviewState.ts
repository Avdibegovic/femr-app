import { IPulseOverviewItem } from "../../model/PulseOverviewItme";

export interface PulseOverviewState {
  pulseOverview: IPulseOverviewItem[];
  pulseOverviewLoading: boolean;
}
