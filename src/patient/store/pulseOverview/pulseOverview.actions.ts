import { createAction } from "@reduxjs/toolkit";
import { IPulseOverviewItem } from "../../model/PulseOverviewItme";

export const getPulseOverview = createAction("[Get Pulse Overview]");

export const getPulseOverviewSuccess = createAction(
  "[Get Pulse Overview] Success",
  (pulseOverview: IPulseOverviewItem[]) => ({ payload: pulseOverview })
);

export const getPulseOverviewFail = createAction(
  "[Get Pulse Overview] Fail",
  (error: any) => ({ payload: error })
);
