import { createAction } from "@reduxjs/toolkit";

export const insertVitals = createAction("[Insert Vitals]");

export const insertVitalsSuccess = createAction("[Insert Vitals] uccess");

export const insertVitalsFail = createAction(
  "[Insert Vitals] fail",
  (error: any) => ({ payload: error })
);
