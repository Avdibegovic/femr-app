import { createReducer } from "@reduxjs/toolkit";
import { AppInitialState } from "../../../base/store/initial/AppInitialState";
import {
  insertVitals,
  insertVitalsFail,
  insertVitalsSuccess,
} from "./vitalsInput.actions";
import { VitalsInputState } from "./VitalsInputState";

const initialState: VitalsInputState = AppInitialState.vitalsInput;

export const vitalsInputReducer = createReducer(initialState, (builder) => {
  builder.addCase(insertVitals, (currentState) => {
    return {
      ...currentState,
      isLoading: true,
    };
  });
  builder.addCase(insertVitalsSuccess, (currentState) => {
    return {
      ...currentState,
      isLoading: false,
    };
  });
  builder.addCase(insertVitalsFail, (currentState, action) => {
    return {
      ...currentState,
      isLoading: false,
      error: action.payload,
    };
  });
});
