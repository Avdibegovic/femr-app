import { createAction } from "@reduxjs/toolkit";
import { IPso2OverviewItem } from "../../model/Pso2OverviewItem";

export const getPso2Overview = createAction("[Get Pso2 Overview]");
export const getPso2OverviewSuccess = createAction(
  "[Get Pso2 Overview] Success",
  (pso2Overview: IPso2OverviewItem[]) => ({
    payload: pso2Overview,
  })
);
export const getPso2OverviewFail = createAction(
  "[Get Pso2 Overview] Faile",
  (error: any) => ({
    payload: error,
  })
);
