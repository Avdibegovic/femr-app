import { createReducer } from "@reduxjs/toolkit";
import { AppInitialState } from "../../../base/store/initial/AppInitialState";
import {
  getPso2Overview,
  getPso2OverviewFail,
  getPso2OverviewSuccess,
} from "./pso2Overview.actions";
import { Pso2OverviewState } from "./Pso2OverviewState";

const initialState: Pso2OverviewState = AppInitialState.pso2Overview;

export const pso2OverviewReducer = createReducer(initialState, (builder) => {
  builder.addCase(getPso2Overview, (currentState) => {
    return {
      ...currentState,
      error: null,
      pso2OverviewLoading: true,
    };
  });
  builder.addCase(getPso2OverviewSuccess, (currentState, action) => {
    return {
      ...currentState,
      pso2OverviewLoading: false,
      pso2Overview: action.payload,
    };
  });
  builder.addCase(getPso2OverviewFail, (currentState, action) => {
    return {
      ...currentState,
      pso2OverviewLoading: false,
      error: action.payload,
    };
  });
});
