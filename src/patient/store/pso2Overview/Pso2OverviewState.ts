import { IPso2OverviewItem } from "../../model/Pso2OverviewItem";

export interface Pso2OverviewState {
  pso2Overview: IPso2OverviewItem[];
  pso2OverviewLoading: boolean;
}
