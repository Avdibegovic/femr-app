import { createReducer } from "@reduxjs/toolkit";
import { AppInitialState } from "../../../base/store/initial/AppInitialState";
import { IPatientVitals } from "../../model/PatientVitals";
import {
  getLatestPatientVitals,
  getLatestPatientVitalsFail,
  getLatestPatientVitalsSuccess,
} from "./patientVitals.actions";
import { PatientVitalsState } from "./PatientVitalsState";

const initialState: PatientVitalsState = AppInitialState.patientVitals;

export const patientVitalsReducer = createReducer(initialState, (builder) => {
  builder.addCase(getLatestPatientVitals, (currentState) => {
    return {
      ...currentState,
      patientVitalsLoading: true,
    };
  });
  builder.addCase(getLatestPatientVitalsSuccess, (currentState, action) => {
    return {
      ...currentState,
      latestPatientVitals: action.payload,
      patientVitalsLoading: false,
    };
  });
  builder.addCase(getLatestPatientVitalsFail, (currentState, action) => {
    return {
      ...currentState,
      latestPatientVitals: {} as IPatientVitals,
      patientVitalsLoading: false,
      error: action.payload,
    };
  });
});
