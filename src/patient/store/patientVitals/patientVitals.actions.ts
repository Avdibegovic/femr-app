import { createAction } from "@reduxjs/toolkit";
import { IPatientVitals } from "../../model/PatientVitals";

export const getLatestPatientVitals = createAction(
  "[Get Latest Patient Vitals]"
);
export const getLatestPatientVitalsSuccess = createAction(
  "[Get Latest Patient Vitals] Success",
  (latestPatientVitals: IPatientVitals) => ({
    payload: latestPatientVitals,
  })
);
export const getLatestPatientVitalsFail = createAction(
  "[Get Latest Patient Vitals] Fail",
  (error: any) => ({ payload: error })
);
