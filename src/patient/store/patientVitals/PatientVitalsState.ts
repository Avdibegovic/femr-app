import { IPatientVitals } from "../../model/PatientVitals";

export interface PatientVitalsState {
  latestPatientVitals: IPatientVitals;
  patientVitalsLoading: boolean;
}
