import { createReducer } from "@reduxjs/toolkit";
import { AppInitialState } from "../../../base/store/initial/AppInitialState";
import { IPatient } from "../../model/Patient";
import {
  getPatient,
  getPatientFail,
  getPatientSuccess,
} from "./patient.actions";
import { PatientState } from "./PatientState";

const initialState: PatientState = AppInitialState.patient;

export const patientReducer = createReducer(initialState, (builder) => {
  builder.addCase(getPatient, (currentState) => {
    return {
      ...currentState,
      patientLoading: true,
    };
  });
  builder.addCase(getPatientSuccess, (currentState, builder) => {
    return {
      ...currentState,
      patientLoading: false,
      patient: builder.payload,
    };
  });
  builder.addCase(getPatientFail, (currentState, builder) => {
    return {
      ...currentState,
      patientLoading: false,
      patient: {} as IPatient,
      error: builder.payload,
    };
  });
});
