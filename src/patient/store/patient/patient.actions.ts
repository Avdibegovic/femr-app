import { createAction } from "@reduxjs/toolkit";
import { IPatient } from "../../model/Patient";

export const getPatient = createAction("[Get Patient]");
export const getPatientSuccess = createAction(
  "[Get Patient] Success",
  (patient: IPatient) => ({
    payload: patient,
  })
);
export const getPatientFail = createAction(
  "[Get Patient] Fail",
  (error: any) => ({ payload: error })
);
