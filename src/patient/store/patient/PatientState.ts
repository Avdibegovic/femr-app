import { IPatient } from "../../model/Patient";

export interface PatientState {
  patient: IPatient;
  patientLoading: boolean;
}
