import { ITemperatureOverviewItem } from "../../model/TemperatureOverviewItem";

export interface TemperatureOverviewState {
  temperatureOverview: ITemperatureOverviewItem[];
  temperatureOverviewLoading: boolean;
}
