import { createAction } from "@reduxjs/toolkit";
import { ITemperatureOverviewItem } from "../../model/TemperatureOverviewItem";

export const getTemperatureOverview = createAction(
  "[Get Temperature Overview]"
);
export const getTemperatureOverviewSuccess = createAction(
  "[Get Temperature Overview] Success",
  (temperatureOverview: ITemperatureOverviewItem[]) => ({
    payload: temperatureOverview,
  })
);
export const getTemperatureOverviewFail = createAction(
  "[Get Temperature Overview] Fail",
  (error: any) => ({
    payload: error,
  })
);
