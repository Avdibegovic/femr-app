import { createReducer } from "@reduxjs/toolkit";
import { AppInitialState } from "../../../base/store/initial/AppInitialState";
import {
  getTemperatureOverview,
  getTemperatureOverviewFail,
  getTemperatureOverviewSuccess,
} from "./temperatureOverview.actions";
import { TemperatureOverviewState } from "./TemperatureOverviewState";

const initialState: TemperatureOverviewState =
  AppInitialState.temperatureOverview;

export const temperatureOverviewReducer = createReducer(
  initialState,
  (builder) => {
    builder.addCase(getTemperatureOverview, (currentState) => {
      return {
        ...currentState,
        error: null,
        temperatureOverviewLoading: true,
      };
    });
    builder.addCase(getTemperatureOverviewSuccess, (currentState, action) => {
      return {
        ...currentState,
        temperatureOverviewLoading: false,
        temperatureOverview: action.payload,
      };
    });
    builder.addCase(getTemperatureOverviewFail, (currentState, action) => {
      return {
        ...currentState,
        temperatureOverviewLoading: false,
        error: action.payload,
      };
    });
  }
);
