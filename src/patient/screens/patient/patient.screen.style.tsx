import { StyleSheet } from "react-native";

export const PatientScreenStyle = StyleSheet.create({
  patientlistItemContainerStyle: {
    borderRadius: 40,
    borderWidth: 1,
    borderBottomWidth: 1,
    borderColor: "#9bd3ae",
    backgroundColor: "white",
    marginTop: 5,
    marginBottom: 5,
    marginLeft: 10,
    marginRight: 10,
  },
  tabBar: {
    backgroundColor: "none",
  },
  tab: {
    backgroundColor: "white",
    borderWidth: 1,
    borderColor: "#9bd3ae",
    borderRadius: 40,
    padding: 0,
    marginLeft: 10,
    marginBottom: 10,
    flexShrink: 0,
  },
  selectedTab: {
    backgroundColor: "#9bd3ae",
    color: "white",
  },
});
