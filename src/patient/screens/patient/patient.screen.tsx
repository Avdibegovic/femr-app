import React from "react";
import { bindActionCreators } from "@reduxjs/toolkit";
import { RefreshControl, Text } from "react-native";
import AppLayout from "../../../base/components/layout/app.layout";
import { AppState } from "../../../base/store/initial/AppState";
import { connect } from "react-redux";
import { Divider, ListItem, Tab, TabView } from "@rneui/themed";
import { ScrollView, TouchableOpacity } from "react-native-gesture-handler";
import ProfileAvatar from "../../../base/components/avatar/profileAvatar.component";
import { PatientScreenStyle } from "./patient.screen.style";
import { PatientState } from "../../store/patient/PatientState";
import {
  getPatient,
  getPatientFail,
  getPatientSuccess,
} from "../../store/patient/patient.actions";
import { useEffect } from "react";
import PatientService from "../../service/PatientService";
import { IPatient } from "../../model/Patient";
import PatientInfoTab from "./components/patientInfoTab.component";
import { showMessage } from "react-native-flash-message";
import Loader from "../../../base/components/loader/loader.component";
import PatientVitalsTabComponent from "./components/patientVitalsTab.component";
import PatientLabTabComponent from "./components/patientLabTab.component";
import PatientPrescriptionsTabComponent from "./components/patientPrescriptionsTab.component";

interface IPatientScreenProps {
  navigation: any;
  route: any;
  patientState: PatientState;

  getPatient: Function;
  getPatientSuccess: Function;
  getPatientFail: Function;
}

const { patientlistItemContainerStyle, tab, tabBar, selectedTab } =
  PatientScreenStyle;

const PatientScreen = (props: IPatientScreenProps) => {
  const {
    navigation,
    route,
    patientState,
    getPatient,
    getPatientFail,
    getPatientSuccess,
  } = props;

  const { patientId } = route.params;

  const [selectedIndex, setSelectedIndex] = React.useState(0);

  const loadPatientData = () => {
    getPatient();
    PatientService()
      .getPatient(patientId)
      .then((patient: IPatient) => getPatientSuccess(patient))
      .catch((error: any) => {
        getPatientFail(error);
        showMessage({ message: error.message, type: "danger" });
      });
  };

  useEffect(() => {
    loadPatientData();
  }, []);

  return (
    <AppLayout>
      {patientState.patientLoading ? (
        <Loader />
      ) : (
        <>
          <ListItem
            Component={TouchableOpacity}
            containerStyle={patientlistItemContainerStyle}
            disabledStyle={{ opacity: 0.5 }}
            pad={20}
          >
            <ProfileAvatar
              firstname={patientState.patient.firstname}
              lastname={patientState.patient.lastname}
            />
            <ListItem.Content>
              <ListItem.Title>
                <Text>{`${patientState.patient.firstname} ${patientState.patient.lastname}`}</Text>
              </ListItem.Title>
            </ListItem.Content>
          </ListItem>
          <Divider
            style={{
              marginTop: 10,
              marginBottom: 10,
              borderWidth: 1,
              borderColor: "#9bd3ae",
            }}
          />
          <Tab
            containerStyle={tabBar}
            value={selectedIndex}
            onChange={(e) => setSelectedIndex(e)}
            disableIndicator={true}
            scrollable={true}
          >
            <Tab.Item
              containerStyle={
                selectedIndex === 0 ? { ...tab, ...selectedTab } : tab
              }
              title="Informacije"
              titleStyle={{ fontSize: 15 }}
            />
            <Tab.Item
              containerStyle={
                selectedIndex === 1 ? { ...tab, ...selectedTab } : tab
              }
              title="Vitalni parametri"
              titleStyle={{ fontSize: 15 }}
            />
            <Tab.Item
              containerStyle={
                selectedIndex === 2 ? { ...tab, ...selectedTab } : tab
              }
              title="Lab nalazi"
              titleStyle={{ fontSize: 15 }}
            />
            <Tab.Item
              containerStyle={
                selectedIndex === 3 ? { ...tab, ...selectedTab } : tab
              }
              title="Terapija"
              titleStyle={{ fontSize: 15 }}
            />
          </Tab>
          <TabView
            value={selectedIndex}
            onChange={(index) => setSelectedIndex(index)}
            animationType="spring"
          >
            <TabView.Item style={{ width: "100%" }}>
              <PatientInfoTab patient={patientState.patient} />
            </TabView.Item>
            <TabView.Item style={{ width: "100%" }}>
              <PatientVitalsTabComponent
                navigation={navigation}
                patientId={patientId}
              />
            </TabView.Item>
            <TabView.Item style={{ width: "100%" }}>
              <PatientLabTabComponent patientId={patientId} />
            </TabView.Item>
            <TabView.Item style={{ width: "100%" }}>
              <PatientPrescriptionsTabComponent patientId={patientId} />
            </TabView.Item>
          </TabView>
        </>
      )}
    </AppLayout>
  );
};

const mapStateToProps = (store: AppState) => ({
  patientState: store.patient,
});

const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      getPatient: getPatient,
      getPatientSuccess: getPatientSuccess,
      getPatientFail: getPatientFail,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(PatientScreen);
