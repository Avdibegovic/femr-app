import { Icon } from "@rneui/themed";
import React from "react";
import { RefreshControl, ScrollView } from "react-native";
import SelectDropdown from "react-native-select-dropdown";
import { IPatientLabMeasurement } from "../../../model/PatientLabMeasurement";
import LabItemMeasurementsOverview from "./labItemMeasurementsOverview.component";
import { PatientLabTabStyle } from "./patientLabTab.component.style";

interface Props {
  labResultCodes: string[];
  labMeasurements?: IPatientLabMeasurement[];
  onSelectedResultCodeChange(resultCode: string): void;
  refreshing: boolean;
  onRefresh: () => void;
}

const { resultCodeSelectStyle } = PatientLabTabStyle;

const LabItemOverview = (props: Props) => {
  const {
    labResultCodes,
    labMeasurements,
    onSelectedResultCodeChange,
    refreshing,
    onRefresh,
  } = props;

  return (
    <>
      <SelectDropdown
        buttonStyle={resultCodeSelectStyle}
        defaultValue={labResultCodes[0]}
        data={labResultCodes}
        buttonTextAfterSelection={(selectedItem) => selectedItem}
        rowTextForSelection={(item) => item}
        onSelect={(selectedItem) => onSelectedResultCodeChange(selectedItem)}
        dropdownIconPosition={"right"}
        renderDropdownIcon={(isOpened) => (
          <Icon
            name={isOpened ? "arrow-up" : "arrow-down"}
            type="simple-line-icon"
          />
        )}
      />
      <ScrollView
        contentContainerStyle={{
          flexGrow: 1,
          justifyContent: "center",
        }}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
      >
        <LabItemMeasurementsOverview
          labMeasurements={labMeasurements || ([] as IPatientLabMeasurement[])}
        />
      </ScrollView>
    </>
  );
};

export default LabItemOverview;
