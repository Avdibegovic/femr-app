import React from "react";
import { bindActionCreators } from "@reduxjs/toolkit";
import { useEffect } from "react";
import { RefreshControl, ScrollView, View } from "react-native";
import { showMessage } from "react-native-flash-message";
import { connect } from "react-redux";
import Loader from "../../../../base/components/loader/loader.component";
import { AppState } from "../../../../base/store/initial/AppState";
import { IPatientPrescription } from "../../../model/PatientPrescription";
import PatientPrescriptionService from "../../../service/PatientPrescriptionService";
import {
  getPatientPrescriptions,
  getPatientPrescriptionsFail,
  getPatientPrescriptionsSuccess,
} from "../../../store/patientPrescriptions/patientPrescriptions.actions";
import PatientPrescriptionsOverview from "./patientPrescriptionsOverview.component";

interface IPatientPrescriptionsTabProps {
  patientId: number;

  patientPrescriptionsLoading: boolean;
  patientPrescriptions: IPatientPrescription[];

  getPatientPrescriptions: Function;
  getPatientPrescriptionsSuccess: Function;
  getPatientPrescriptionsFails: Function;
}

const PatientPrescriptionsTab = (props: IPatientPrescriptionsTabProps) => {
  const {
    patientId,
    patientPrescriptionsLoading,
    patientPrescriptions,
    getPatientPrescriptions,
    getPatientPrescriptionsSuccess,
    getPatientPrescriptionsFails,
  } = props;

  const [refreshing, setRefreshing] = React.useState(false);

  const loadPrescriptions = () => {
    getPatientPrescriptions();

    PatientPrescriptionService()
      .getActivePrescriptions(patientId)
      .then((patientPrescriptions: IPatientPrescription[]) =>
        getPatientPrescriptionsSuccess(patientPrescriptions)
      )
      .catch((error: any) => {
        getPatientPrescriptionsFails(error);
        showMessage({ message: error.message, type: "danger" });
      });
  };

  useEffect(() => {
    loadPrescriptions();
  }, []);

  const onRefresh = () => {
    loadPrescriptions();
    setRefreshing(false);
  };

  if (patientPrescriptionsLoading) {
    return <Loader />;
  }

  return (
    <ScrollView
      contentContainerStyle={{
        flexGrow: 1,
      }}
      refreshControl={
        <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
      }
    >
      <PatientPrescriptionsOverview
        patientPrescriptions={patientPrescriptions}
      />
    </ScrollView>
  );
};

const mapStateToProps = (store: AppState) => ({
  patientPrescriptionsLoading:
    store.patientPrescriptions.patientPrescriptionsLoading,
  patientPrescriptions: store.patientPrescriptions.patientPrescriptions,
});

const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      getPatientPrescriptions: getPatientPrescriptions,
      getPatientPrescriptionsSuccess: getPatientPrescriptionsSuccess,
      getPatientPrescriptionsFails: getPatientPrescriptionsFail,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PatientPrescriptionsTab);
