import { Layout } from "@ui-kitten/components";
import React from "react";
import InfroItem from "../../../../base/components/info/infoItem.component";
import DataUtils from "../../../../base/utils/DataUtils";
import DateUtils from "../../../../base/utils/DateUtils";
import { IPatient } from "../../../model/Patient";
import { PatientInfoTabStyle } from "./patientInforTab.component.styles";

export interface Props {
  patient: IPatient;
}

const { infoItem } = PatientInfoTabStyle;

const PatientInfoTab = (props: Props) => {
  const { patient } = props;

  return (
    <Layout>
      <InfroItem
        style={infoItem}
        key={"firstname"}
        label="Ime"
        value={patient.firstname}
      />
      <InfroItem
        style={infoItem}
        key={"lastname"}
        label="Prezime"
        value={patient.lastname}
      />
      <InfroItem
        style={infoItem}
        key={"sex"}
        label="Spol"
        value={DataUtils().decodeSex(patient.sex)}
      />
      <InfroItem
        style={infoItem}
        key={"dateOfBirth"}
        label="Datum rođenja"
        value={DateUtils().formatDate(patient.dateOfBirth)}
      />
      <InfroItem
        style={infoItem}
        key={"department"}
        label="Odijel"
        value={DataUtils().decodeDepartment(patient.department)}
      />
      <InfroItem
        style={{
          ...infoItem,
          flexDirection: "column",
          alignItems: "flex-start",
        }}
        key={"importantNoticeo"}
        label="Napomena"
        value={patient.importantNotice}
      />
    </Layout>
  );
};

export default PatientInfoTab;
