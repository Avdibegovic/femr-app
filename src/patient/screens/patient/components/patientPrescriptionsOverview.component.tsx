import { Layout, Text } from "@ui-kitten/components";
import React from "react";
import { ScrollView } from "react-native";
import { IPatientPrescription } from "../../../model/PatientPrescription";
import { PatientPrescriptionsOverviewStyle } from "./patientPrescriptionsOverview.component.style";

export interface Props {
  patientPrescriptions: IPatientPrescription[];
}

const { itemStyle, headerStyle } = PatientPrescriptionsOverviewStyle;

const PatientPrescriptionsOverview = (props: Props) => {
  const { patientPrescriptions } = props;

  const renderPrescriptions = () => {
    if (patientPrescriptions.length === 0) {
      return <Text>{""}</Text>;
    }
    return patientPrescriptions.map(
      (prescription: IPatientPrescription, index: number) => {
        return (
          <Layout key={index} level={"1"} style={itemStyle}>
            <Text
              style={{ width: "25%", padding: 5 }}
              category="h6"
              appearance={"hint"}
            >
              {prescription.drug}
            </Text>
            <Text
              style={{ width: "25%", padding: 5 }}
              category="h6"
              appearance={"hint"}
            >{`${prescription.dosage || ""} ${
              prescription.drugFormName || ""
            } ${prescription.drugRouteName || ""}`}</Text>
            <Text
              style={{ width: "25%", padding: 5 }}
              category="h6"
              appearance={"hint"}
            >
              {prescription.drugIntervalName || ""}
            </Text>
            <Text
              style={{ width: "25%", padding: 5 }}
              category="h6"
              appearance={"hint"}
            >
              {prescription.note || ""}
            </Text>
          </Layout>
        );
      }
    );
  };
  return (
    <>
      <Layout level={"1"} style={itemStyle}>
        <Text
          style={{ ...headerStyle, width: "25%", padding: 5 }}
          category="h6"
        >
          {"Lijek"}
        </Text>
        <Text
          style={{ ...headerStyle, width: "25%", padding: 5 }}
          category="h6"
        >{`Primjena`}</Text>
        <Text
          style={{ ...headerStyle, width: "25%", padding: 5 }}
          category="h6"
        >{`Inteval`}</Text>
        <Text
          style={{ ...headerStyle, width: "25%", padding: 5 }}
          category="h6"
        >{`Komentar`}</Text>
      </Layout>
      {renderPrescriptions()}
    </>
  );
};

export default PatientPrescriptionsOverview;
