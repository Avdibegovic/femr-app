import React, { useEffect } from "react";
import { bindActionCreators } from "@reduxjs/toolkit";
import { Text } from "@rneui/themed";
import { showMessage } from "react-native-flash-message";
import { connect } from "react-redux";
import Loader from "../../../../base/components/loader/loader.component";
import { AppState } from "../../../../base/store/initial/AppState";
import { IPatientVitals } from "../../../model/PatientVitals";
import PatientVitalsService from "../../../service/PatientVitalsService";
import {
  getLatestPatientVitals,
  getLatestPatientVitalsFail,
  getLatestPatientVitalsSuccess,
} from "../../../store/patientVitals/patientVitals.actions";
import { patientVitalsTabStyle } from "./patientVitalsTab.component.style";
import {
  RefreshControl,
  ScrollView,
  TouchableOpacity,
  View,
} from "react-native";
import * as ScreenOrientation from "expo-screen-orientation";
import { FAB } from "@rneui/base";
import TemperatureUtils from "../../../../base/utils/TemperatureUtils";
import VitalCard from "./vitalCard.component";

interface PatientVitalsTabProps {
  navigation: any;
  patientId: number;

  getLatestPatientVitals: Function;
  getLatestPatientVitalsSuccess: Function;
  getLatestPatientVitalsFail: Function;

  patientVitalsLoading: boolean;
  latestPatientVitals: IPatientVitals;
}

const PatientVitalsTab = (props: PatientVitalsTabProps) => {
  const {
    navigation,
    patientId,
    patientVitalsLoading,
    latestPatientVitals,
    getLatestPatientVitalsSuccess,
    getLatestPatientVitalsFail,
  } = props;

  const [refreshing, setRefreshing] = React.useState(false);

  const loadVitals = () => {
    PatientVitalsService()
      .getLatestPatientVitals(patientId)
      .then((latestPatientVitals: IPatientVitals) => {
        getLatestPatientVitalsSuccess(latestPatientVitals);
      })
      .catch((error: any) => {
        getLatestPatientVitalsFail(error);
        showMessage({ message: error.message, type: "danger" });
      });
  };

  useEffect(() => {
    loadVitals();
  }, []);

  useEffect(() => {
    loadVitals();
  }, [patientVitalsLoading]);

  const onRefresh = () => {
    loadVitals();
    setRefreshing(false);
  };

  return patientVitalsLoading ? (
    <Loader />
  ) : (
    <View>
      <FAB
        style={patientVitalsTabStyle.fab}
        icon={{ name: "add", color: "white" }}
        placement={"right"}
        color="green"
        onPress={() => navigation.navigate("VitalsInput", { patientId })}
      />

      <ScrollView
        contentContainerStyle={{
          flexGrow: 1,
          justifyContent: "center",
        }}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
      >
        <TouchableOpacity
          onPress={() => {
            ScreenOrientation.lockAsync(
              ScreenOrientation.OrientationLock.LANDSCAPE_RIGHT
            ).then(() =>
              navigation.navigate("BloodPressureOverview", { patientId })
            );
          }}
        >
          <VitalCard
            date={latestPatientVitals.bloodPressureDate}
            title={"Krvni pritisak"}
          >
            <Text style={patientVitalsTabStyle.label}>{`BPS:`}</Text>
            <Text h4={true}>{latestPatientVitals.bps}</Text>
            <Text style={patientVitalsTabStyle.label}>{`BPD:`}</Text>
            <Text h4={true}>{latestPatientVitals.bpd}</Text>
          </VitalCard>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            ScreenOrientation.lockAsync(
              ScreenOrientation.OrientationLock.LANDSCAPE_RIGHT
            ).then(() => navigation.navigate("PulseOverview", { patientId }));
          }}
        >
          <VitalCard date={latestPatientVitals.pulseDate} title="Pulse">
            <Text h4={true}>{latestPatientVitals.pulse}</Text>
          </VitalCard>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            ScreenOrientation.lockAsync(
              ScreenOrientation.OrientationLock.LANDSCAPE_RIGHT
            ).then(() =>
              navigation.navigate("TemperatureOverview", { patientId })
            );
          }}
        >
          <VitalCard
            date={latestPatientVitals.temperatureDate}
            title={"Temperatura"}
          >
            <Text h4={true}>
              {TemperatureUtils().convertToCelsius(
                latestPatientVitals.temperature
              )}
            </Text>
          </VitalCard>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            ScreenOrientation.lockAsync(
              ScreenOrientation.OrientationLock.LANDSCAPE_RIGHT
            ).then(() => navigation.navigate("Pso2Overview", { patientId }));
          }}
        >
          <VitalCard date={latestPatientVitals.pso2Date} title={"SPO2"}>
            <Text h4={true}>{latestPatientVitals.pso2}</Text>
          </VitalCard>
        </TouchableOpacity>
        <VitalCard date={latestPatientVitals.hydrationDate} title="Hidracija">
          <Text style={patientVitalsTabStyle.label}>{`Zadnja:`}</Text>
          <Text h4={true}>{latestPatientVitals.hydration}</Text>
          <Text style={patientVitalsTabStyle.label}>{`Posljednjih 24h:`}</Text>
          <Text h4={true}>{latestPatientVitals.last24hHydration || 0}</Text>
        </VitalCard>
        <VitalCard date={latestPatientVitals.diuresisDate} title={"Diureza"}>
          <Text h4={true}>{latestPatientVitals.diuresis}</Text>
        </VitalCard>
        <VitalCard date={latestPatientVitals.bloodGlucoseDate} title={"Šuk"}>
          <Text h4={true}>{latestPatientVitals.bloodGlucose}</Text>
        </VitalCard>
      </ScrollView>
    </View>
  );
};

const mapStateToProps = (store: AppState) => ({
  patientVitalsLoading: store.patientVitals.patientVitalsLoading,
  latestPatientVitals: store.patientVitals.latestPatientVitals,
});

const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      getLatestPatientVitals: getLatestPatientVitals,
      getLatestPatientVitalsSuccess: getLatestPatientVitalsSuccess,
      getLatestPatientVitalsFail: getLatestPatientVitalsFail,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(PatientVitalsTab);
