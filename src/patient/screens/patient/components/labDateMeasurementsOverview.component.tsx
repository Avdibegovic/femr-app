import { Layout, Text } from "@ui-kitten/components";
import React from "react";
import { IPatientLabMeasurement } from "../../../model/PatientLabMeasurement";
import { LabMeasurementOverviewStyle } from "./labMeasurementOverview.component.style";

interface Props {
  labMeasurements: IPatientLabMeasurement[];
}

const { itemStyle, headerStyle } = LabMeasurementOverviewStyle;

const LabDateMeasurementsOverview = (props: Props) => {
  const { labMeasurements } = props;

  const renderMeasurementItems = () => {
    return labMeasurements.map(
      (labMeasurement: IPatientLabMeasurement, index: number) => {
        const isAbnormal = labMeasurement.abnormal?.length !== 0;
        const referenceValue = labMeasurement?.range || "";
        const units = labMeasurement?.units || "";

        return (
          <Layout key={index} level={"1"} style={itemStyle}>
            <Text style={{ width: "33%" }} appearance={"hint"} category="h6">
              {labMeasurement.resultCode}
            </Text>
            <Text
              style={{
                color: isAbnormal ? "red" : "green",
                textAlign: "center",
                width: "33%",
                fontWeight: isAbnormal ? "bold" : "normal",
              }}
              category="h6"
            >
              {labMeasurement.result}
            </Text>
            <Text
              appearance={"hint"}
              style={{
                textAlign: "right",
                width: "33%",
              }}
              category="h6"
            >
              {`${units}\n${referenceValue}`}
            </Text>
          </Layout>
        );
      }
    );
  };
  return (
    <Layout>
      <Layout level={"1"} style={itemStyle}>
        <Text style={{ ...headerStyle, width: "33%" }} category="h6">
          {"Parametar"}
        </Text>
        <Text
          style={{ ...headerStyle, textAlign: "center", width: "33%" }}
          category="h6"
        >{`Vrijednost`}</Text>
        <Text
          style={{ ...headerStyle, textAlign: "right", width: "33%" }}
          category="h6"
        >{`Jedinica mjere\nRef.`}</Text>
      </Layout>
      {renderMeasurementItems()}
    </Layout>
  );
};

export default LabDateMeasurementsOverview;
