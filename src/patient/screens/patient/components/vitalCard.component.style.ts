import { StyleSheet } from "react-native";

export const vitalCardStyle = StyleSheet.create({
  cardContainer: {
    borderColor: "#9bd3ae",
    borderRadius: 10,
    backgroundColor: "white",
  },
  cardContentContainer: {
    alignItems: "center",
  },
  cardSubtitle: {
    color: "#8F9BB3",
  },
});
