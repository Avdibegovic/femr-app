import { Card } from "@rneui/themed";
import * as React from "react";
import { Text, View } from "react-native";
import DateUtils from "../../../../base/utils/DateUtils";
import { vitalCardStyle } from "./vitalCard.component.style";

export interface Props {
  date?: Date;
  title: string;
  children: any;
}

const VitalCard = (props: Props) => {
  const { date, children, title } = props;

  return (
    <Card containerStyle={vitalCardStyle.cardContainer}>
      <Card.Title h4={true}>{title}</Card.Title>
      <Card.FeaturedSubtitle style={vitalCardStyle.cardSubtitle}>
        {date ? DateUtils().formatDateWithTime(date) : ""}
      </Card.FeaturedSubtitle>
      <Card.Divider />
      <View
        style={[
          vitalCardStyle.cardContentContainer,
          { flex: 1, flexDirection: "row", justifyContent: "center" },
        ]}
      >
        {date ? children : <Text>Nema podataka</Text>}
      </View>
    </Card>
  );
};

export default VitalCard;
