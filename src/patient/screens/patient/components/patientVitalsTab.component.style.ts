import { StyleSheet } from "react-native";

export const patientVitalsTabStyle = StyleSheet.create({
  label: {
    color: "#8F9BB3",
    marginLeft: 10,
    marginRight: 3,
  },
  fab: { zIndex: 100, height: 100, margin: 20 },
});
