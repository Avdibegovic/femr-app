import { Dimensions, StyleSheet } from "react-native";

const SCREEN_WIDTH = Dimensions.get("window").width;

export const PatientLabTabStyle = StyleSheet.create({
  resultCodeSelectStyle: {
    borderRadius: 10,
    marginTop: 20,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 20,
    padding: 12,
    borderWidth: 1,
    borderColor: "#9bd3ae",
    backgroundColor: "white",
    width: SCREEN_WIDTH - 20,
  },
});
