import { StyleSheet } from "react-native";

export const PatientPrescriptionsOverviewStyle = StyleSheet.create({
  itemStyle: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    padding: 16,
    borderBottomWidth: 1,
    borderColor: "#9bd3ae",
  },
  headerStyle: {
    fontWeight: "bold",
  },
});
