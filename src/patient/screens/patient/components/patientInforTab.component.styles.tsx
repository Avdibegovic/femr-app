import { StyleSheet } from "react-native";

export const PatientInfoTabStyle = StyleSheet.create({
  infoItem: {
    padding: 16,
    borderBottomWidth: 1,
    borderColor: "#9bd3ae",
  },
});
