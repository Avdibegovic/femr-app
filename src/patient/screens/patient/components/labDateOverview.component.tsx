import React from "react";
import { IPatientLabMeasurement } from "../../../model/PatientLabMeasurement";
import DatePicker from "../../../../base/components/datePicker/datePicker.component";
import { RefreshControl, ScrollView } from "react-native";
import LabDateMeasurementsOverview from "./labDateMeasurementsOverview.component";

interface Props {
  labDates: Date[];
  labMeasurements?: IPatientLabMeasurement[];
  onLabDateChange: (labDate: Date) => void;
  refreshing: boolean;
  onRefresh: () => void;
}

const LabDateOverview = (props: Props) => {
  const { labDates, labMeasurements, onLabDateChange, refreshing, onRefresh } =
    props;

  const newestDate = labDates[0];
  const oldestDate = labDates[labDates.length - 1];

  return (
    <>
      <DatePicker
        defaultDate={newestDate}
        startDate={oldestDate}
        endDate={newestDate}
        markedDates={labDates}
        onSelect={(selectedDate: Date) => onLabDateChange(selectedDate)}
      />
      <ScrollView
        contentContainerStyle={{
          flexGrow: 1,
          justifyContent: "center",
        }}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
      >
        <LabDateMeasurementsOverview
          labMeasurements={labMeasurements || ([] as IPatientLabMeasurement[])}
        />
      </ScrollView>
    </>
  );
};

export default LabDateOverview;
