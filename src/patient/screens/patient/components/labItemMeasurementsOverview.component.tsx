import { Layout, Text } from "@ui-kitten/components";
import DateUtils from "../../../../base/utils/DateUtils";
import { IPatientLabMeasurement } from "../../../model/PatientLabMeasurement";
import { LabMeasurementOverviewStyle } from "./labMeasurementOverview.component.style";

interface Props {
  labMeasurements: IPatientLabMeasurement[];
}

const { itemStyle, headerStyle } = LabMeasurementOverviewStyle;

const LabItemMeasurementsOverview = (props: Props) => {
  const { labMeasurements } = props;

  const referenceValue = labMeasurements[0]?.range || "";
  const units = labMeasurements[0]?.units || "";

  const renderMeasurementItems = () => {
    return labMeasurements.map(
      (labMeasurement: IPatientLabMeasurement, index: number) => {
        const isAbnormal = labMeasurement.abnormal?.length !== 0;
        return (
          <Layout key={index} level={"1"} style={itemStyle}>
            <Text appearance={"hint"} category="h6">
              {DateUtils().formatDateWithTime(labMeasurement.dateCollected)}
            </Text>
            <Text
              style={{
                color: isAbnormal ? "red" : "green",
                fontWeight: isAbnormal ? "bold" : "normal",
              }}
              category="h6"
            >
              {labMeasurement.result}
            </Text>
          </Layout>
        );
      }
    );
  };
  return (
    <Layout>
      <Layout level={"1"} style={itemStyle}>
        <Text style={headerStyle} category="h6">
          {"Datum i vrijeme"}
        </Text>
        <Text
          style={{ ...headerStyle, textAlign: "right" }}
          category="h6"
        >{`Vrijednost (${units})\n(Ref. ${referenceValue})`}</Text>
      </Layout>
      {renderMeasurementItems()}
    </Layout>
  );
};
export default LabItemMeasurementsOverview;
