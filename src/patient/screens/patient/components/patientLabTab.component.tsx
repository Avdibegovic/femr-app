import React from "react";
import { bindActionCreators } from "@reduxjs/toolkit";
import { Icon } from "@rneui/themed";
import { useEffect } from "react";
import { View } from "react-native";
import { showMessage } from "react-native-flash-message";
import { connect } from "react-redux";
import Loader from "../../../../base/components/loader/loader.component";
import { AppState } from "../../../../base/store/initial/AppState";
import { IPatientLabMeasurement } from "../../../model/PatientLabMeasurement";
import PatientLabService from "../../../service/PatientLabService";
import {
  getLabDates,
  getLabDatesFail,
  getLabDatesSuccess,
  getLabMeasurements,
  getLabMeasurementsFail,
  getLabMeasurementsSuccess,
  getLabResultCodes,
  getLabResultCodesFail,
  getLabResultCodesSuccess,
} from "../../../store/patientLab/patientLab.actions";
import { FloatingAction } from "react-native-floating-action";
import LabItemOverview from "./labItemOverview.component";
import LabDateOverview from "./labDateOverview.component";

const DATE_OVERVIEW_KEY = "date-overview";
const ITEM_OVERVIEW_KEY = "item-overview";

interface PatientLabTabProps {
  patientId: number;

  labLoading: boolean;
  labResultCodes: string[];
  labMeasurements?: IPatientLabMeasurement[];
  labDates: number[];

  getLabResultCodes: Function;
  getLabResultCodesSuccess: Function;
  getLabResultCodesFail: Function;
  getLabMeasurements: Function;
  getLabMeasurementsSuccess: Function;
  getLabMeasurementsFail: Function;
  getLabDates: Function;
  getLabDatesSuccess: Function;
  getLabDatesFail: Function;
}

const PatientLabTab = (props: PatientLabTabProps) => {
  const {
    patientId,
    getLabResultCodes,
    getLabMeasurementsSuccess,
    getLabResultCodesFail,
    getLabMeasurements,
    getLabResultCodesSuccess,
    getLabMeasurementsFail,
    getLabDates,
    getLabDatesFail,
    getLabDatesSuccess,
    labLoading,

    labResultCodes,
    labMeasurements,
    labDates,
  } = props;

  const [overviewType, setOverviewType] = React.useState(DATE_OVERVIEW_KEY);
  const [refreshing, setRefreshing] = React.useState(false);

  const labOverviewTypes = [
    {
      text: "Pregled po datumu",
      icon: <Icon name={"calendar"} type="simple-line-icon" />,
      name: DATE_OVERVIEW_KEY,
      position: 1,
    },
    {
      text: "Pregled po parametru",
      icon: <Icon name={"list"} type="simple-line-icon" />,
      name: ITEM_OVERVIEW_KEY,
      position: 2,
    },
  ];

  const loadResultCodes = () => {
    PatientLabService()
      .getLabResultCodes(patientId)
      .then((labResultCodes: string[]) => {
        getLabResultCodesSuccess(labResultCodes);
      })
      .catch((error: any) => {
        getLabResultCodesFail(error);
        showMessage({ message: error.message, type: "danger" });
      });
  };

  const loadLabDates = () => {
    PatientLabService()
      .getLabDates(patientId)
      .then((labDates: number[]) => {
        onLabDateSelect(new Date(labDates[0]));
        getLabDatesSuccess(labDates);
      })
      .catch((error: any) => {
        getLabDatesFail(error);
        showMessage({ message: error.message, type: "danger" });
      });
  };

  const onResultCodeSelect = (resultCode: string) => {
    getLabMeasurements();
    PatientLabService()
      .getLabItemMeasurements(patientId, resultCode)
      .then((labMeasurements: IPatientLabMeasurement[]) =>
        getLabMeasurementsSuccess(labMeasurements)
      )
      .catch((error: any) => {
        getLabMeasurementsFail(error);
        showMessage({ message: error.message, type: "danger" });
      });
  };

  const onLabDateSelect = (collectionDate: Date) => {
    getLabMeasurements();
    PatientLabService()
      .getLabDateMeasurements(patientId, collectionDate)
      .then((labMeasurements: IPatientLabMeasurement[]) =>
        getLabMeasurementsSuccess(labMeasurements)
      )
      .catch((error: any) => {
        getLabMeasurementsFail(error);
        showMessage({ message: error.message, type: "danger" });
      });
  };

  const onOverviewTypeChange = (name: string) => {
    setOverviewType(name);
    if (name === ITEM_OVERVIEW_KEY) {
      onResultCodeSelect(labResultCodes[0]);
    } else if (name === DATE_OVERVIEW_KEY) {
      onLabDateSelect(new Date(labDates[0]));
    }
  };

  useEffect(() => {
    getLabDates();
    loadLabDates();

    getLabResultCodes();
    loadResultCodes();
  }, []);

  const onRefresh = () => {
    getLabDates();
    loadLabDates();

    getLabResultCodes();
    loadResultCodes();

    setRefreshing(false);
  };

  if (labLoading) {
    return <Loader />;
  }

  return (
    <View style={{ flex: 1 }}>
      {overviewType === ITEM_OVERVIEW_KEY && (
        <LabItemOverview
          labResultCodes={labResultCodes}
          labMeasurements={labMeasurements}
          onSelectedResultCodeChange={(selectedResultCode: string) =>
            onResultCodeSelect(selectedResultCode)
          }
          refreshing={refreshing}
          onRefresh={onRefresh}
        />
      )}
      {overviewType === DATE_OVERVIEW_KEY && (
        <LabDateOverview
          labDates={labDates.map((labDate: number) => new Date(labDate))}
          labMeasurements={labMeasurements}
          onLabDateChange={onLabDateSelect}
          refreshing={refreshing}
          onRefresh={onRefresh}
        />
      )}
      <FloatingAction
        floatingIcon={
          <Icon name={"options"} type="simple-line-icon" color={"white"} />
        }
        color="green"
        tintColor="green"
        actions={labOverviewTypes}
        onPressItem={(name) => {
          name && onOverviewTypeChange(name);
        }}
      />
    </View>
  );
};

const mapStateToProps = (store: AppState) => ({
  labLoading: store.patientLab.labLoading,
  labResultCodes: store.patientLab.resultCodes,
  labMeasurements: store.patientLab.labMeasurements,
  labDates: store.patientLab.labDates,
});

const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      getLabResultCodes: getLabResultCodes,
      getLabResultCodesSuccess: getLabResultCodesSuccess,
      getLabResultCodesFail: getLabResultCodesFail,
      getLabMeasurements: getLabMeasurements,
      getLabMeasurementsSuccess: getLabMeasurementsSuccess,
      getLabMeasurementsFail: getLabMeasurementsFail,
      getLabDates: getLabDates,
      getLabDatesSuccess: getLabDatesSuccess,
      getLabDatesFail: getLabDatesFail,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(PatientLabTab);
