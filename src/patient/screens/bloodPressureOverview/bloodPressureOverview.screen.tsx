import React, { useEffect } from "react";
import { BackHandler, Text } from "react-native";
import * as ScreenOrientation from "expo-screen-orientation";
import AppLayout from "../../../base/components/layout/app.layout";
import { AppState } from "../../../base/store/initial/AppState";
import { bindActionCreators, compose } from "@reduxjs/toolkit";
import {
  getBloodPressureOverview,
  getBloodPressureOverviewFail,
  getBloodPressureOverviewSuccess,
} from "../../store/bloodPressureOverview/bloodPressureOverview.actions";
import { connect } from "react-redux";
import { IBloodPressureOverviewItem } from "../../model/BloodPressureOverviewItem";
import PatientVitalsService from "../../service/PatientVitalsService";
import { showMessage } from "react-native-flash-message";
import BloodPressureChart from "./conponents/bloodPressureChart.component";
import Loader from "../../../base/components/loader/loader.component";
import { BloodPressureOverviewState } from "../../store/bloodPressureOverview/BloodPressureOverviewState";

interface BloodPressureOverviewProps {
  navigation: any;
  route: any;

  bloodPressureOverviewState: BloodPressureOverviewState;

  getBloodPressureOverview: Function;
  getBloodPressureOverviewSuccess: Function;
  getBloodPressureOverviewFail: Function;
}

const BloodPressureOverviewScreen = (props: BloodPressureOverviewProps) => {
  const {
    navigation,
    route,
    bloodPressureOverviewState,
    getBloodPressureOverviewSuccess,
    getBloodPressureOverview,
    getBloodPressureOverviewFail,
  } = props;

  const { patientId } = route.params;

  useEffect(() => {
    getBloodPressureOverview();
    PatientVitalsService()
      .getBloodPressureOverview(patientId)
      .then((bloodPressureOverview: IBloodPressureOverviewItem[]) => {
        getBloodPressureOverviewSuccess(bloodPressureOverview);
      })
      .catch((error) => {
        getBloodPressureOverviewFail(error);
        showMessage({ message: error.message, type: "danger" });
      });
  }, []);

  useEffect(() => {
    const backAction = () => {
      ScreenOrientation.lockAsync(ScreenOrientation.OrientationLock.PORTRAIT);
      navigation.goBack();
      return true;
    };

    BackHandler.addEventListener("hardwareBackPress", backAction);
  }, []);

  return (
    <AppLayout>
      {bloodPressureOverviewState.bloodPressureOverviewLoading ? (
        <Loader />
      ) : (
        <BloodPressureChart
          bloodPressureOverview={
            bloodPressureOverviewState.bloodPressureOverview ||
            ([] as IBloodPressureOverviewItem[])
          }
        />
      )}
    </AppLayout>
  );
};

const mapStateToProps = (store: AppState) => ({
  bloodPressureOverviewState: store.bloodPressureOverview,
});

const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      getBloodPressureOverview: getBloodPressureOverview,
      getBloodPressureOverviewSuccess: getBloodPressureOverviewSuccess,
      getBloodPressureOverviewFail: getBloodPressureOverviewFail,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BloodPressureOverviewScreen);
