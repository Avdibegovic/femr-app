import React from "react";
import { Dimensions, Text, View } from "react-native";
import { LineChart } from "react-native-chart-kit";
import DateUtils from "../../../../base/utils/DateUtils";
import { IBloodPressureOverviewItem } from "../../../model/BloodPressureOverviewItem";
import { Text as TextSVG } from "react-native-svg";

export interface Props {
  bloodPressureOverview: IBloodPressureOverviewItem[];
}

const BloodPressureChart = (props: Props) => {
  const { bloodPressureOverview } = props;

  const chartConfig = {
    backgroundColor: "whitesmoke",
    backgroundGradientFrom: "whitesmoke",
    backgroundGradientTo: "whitesmoke",
    color: (opacity = 1) => `black`,
    labelColor: (opacity = 1) => `black`,
    style: {
      borderRadius: 16,
    },
    propsForDots: {
      r: "6",
      strokeWidth: "2",
      stroke: "#ffa726",
    },
  };

  const labels = bloodPressureOverview.map(
    (item: IBloodPressureOverviewItem) => {
      return DateUtils().formatDateWithTime(item.date);
    }
  );

  const bps = bloodPressureOverview.map((item: IBloodPressureOverviewItem) => {
    return item.bps;
  });

  const bpd = bloodPressureOverview.map((item: IBloodPressureOverviewItem) => {
    return item.bpd;
  });

  const data = {
    labels: labels.length !== 0 ? labels : ["Nema podataka"],
    datasets: [
      {
        data: bps.length !== 0 ? bps : [0],
        color: (opacity = 1) => `red`,
        strokeWidth: 2,
      },
      {
        data: bpd.length !== 0 ? bpd : [0],
        color: (opacity = 1) => `blue`,
        strokeWidth: 2,
      },
    ],
    legend: ["bps", "bpd"],
  };

  return (
    <View>
      <LineChart
        data={data}
        chartConfig={chartConfig}
        height={Dimensions.get("screen").height - 100}
        width={Dimensions.get("screen").width - 30}
        renderDotContent={({ x, y, index, indexData }) => {
          return (
            <TextSVG
              key={`${x} - ${y} - ${index}`}
              x={x}
              y={y - 10}
              fill="black"
              fontSize="16"
              fontWeight="normal"
              textAnchor="middle"
            >
              {indexData}
            </TextSVG>
          );
        }}
      />
    </View>
  );
};

export default BloodPressureChart;
