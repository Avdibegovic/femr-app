import { StyleSheet } from "react-native";

export const PatientSearchScreenStyle = StyleSheet.create({
  searchContainer: {
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 15,
    boder: 0,
    borderTopWidth: 0,
    borderBottomWidth: 0,
  },
  searchInputContainerStyle: {
    borderRadius: 40,
    borderWidth: 1,
    borderBottomWidth: 1,
    borderColor: "#9bd3ae",
    height: 50,
    backgroundColor: "white",
  },
  searchIcon: {
    color: "#ed1c24",
    marginLeft: 10,
  },
  patientListStyle: {
    marginBottom: 80,
  },
  patientlistItemContainerStyle: {
    borderRadius: 40,
    borderWidth: 1,
    borderBottomWidth: 1,
    borderColor: "#9bd3ae",
    backgroundColor: "white",
    marginTop: 5,
    marginBottom: 5,
    marginLeft: 10,
    marginRight: 10,
  },
  roomNumberSytle: {
    borderRadius: 10,
    margin: 10,
    padding: 10,
    backgroundColor: "#9bd3ae",
    fontWeight: "bold",
    fontSize: 23,
    color: "white",
    textAlign: "right",
  },
});
