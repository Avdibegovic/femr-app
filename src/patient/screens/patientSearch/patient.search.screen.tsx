import React, { useEffect, useState } from "react";
import { Divider, SearchBar, Tab } from "@rneui/themed";
import AppLayout from "../../../base/components/layout/app.layout";
import { PatientSearchScreenStyle } from "./patient.search.screen.style";
import { AppState } from "../../../base/store/initial/AppState";
import { bindActionCreators } from "@reduxjs/toolkit";
import {
  getRecentPatients,
  getRecentPatientsSuccess,
  getRecentPatientsFail,
  searchPatients,
  searchPatientsFail,
  searchPatientsSuccess,
  activateSearchLoader,
  deactivateSearchLoader,
  resetPatients,
} from "../../store/patientSearch/patientSearch.actions";
import { connect } from "react-redux";
import PatientService from "../../service/PatientService";
import { IPatient } from "../../model/Patient";
import { showMessage } from "react-native-flash-message";
import { PatientSearchState } from "../../store/patientSearch/PatientSearchState";
import PatientsList from "../patientSearch/components/patientsList.component";
import { GestureResponderEvent } from "react-native";
import CredentialsService from "../../../base/service/CredentialsService";
import { PatientScreenStyle } from "../patient/patient.screen.style";
import {
  FacilityConstants,
  PatientStatusConstants,
} from "../../../base/constants/Constants";
import patientRoomListMapper from "../../model/mappers/PatientRoomListMapper";
import { IPatientSearchParams } from "../../model/PatientSearchParams";

interface PatientSearchScreenProps {
  navigation: any;

  patientSearchState: PatientSearchState;

  searchPatients: Function;
  searchPatientsSuccess: Function;
  searchPatientsFail: Function;
  isSearching: boolean;
  patients: IPatient[];
  activateSearchLoader: Function;
  deactivateSearchLoader: Function;
  resetPatients: Function;

  getRecentPatients: Function;
  getRecentPatientsSuccess: Function;
  getRecentPatientsFail: Function;
  recentPatientsLoading: boolean;
  recentPatients: IPatient[];
}

const { tab, tabBar, selectedTab } = PatientScreenStyle;

const { searchContainer, searchIcon, searchInputContainerStyle } =
  PatientSearchScreenStyle;

const PatientSearchScreen = (props: PatientSearchScreenProps) => {
  const {
    navigation,
    patientSearchState,
    searchPatientsSuccess,
    searchPatientsFail,
    activateSearchLoader,
    deactivateSearchLoader,
  } = props;

  const { patientRooms, recentPatientsLoading, patientsLoading } =
    patientSearchState;

  const [searchTerm, setSearchTerm] = useState("");

  const [selectedFacilityIndex, setSelectedFacilityIndex] = React.useState<
    number | undefined
  >(0);

  useEffect(() => {
    getPatients({
      startsWith: undefined,
      facility: FacilityConstants.FACILITIES_LIST[selectedFacilityIndex || 0],
      status: PatientStatusConstants.ACTIVE,
    });
  }, []);

  const getPatients = (params: IPatientSearchParams) => {
    activateSearchLoader();
    PatientService()
      .getPatients(params)
      .then((patients: IPatient[]) => {
        const patientRooms = patientRoomListMapper(patients);
        searchPatientsSuccess(patientRooms);
        deactivateSearchLoader();
      })
      .catch((error) => {
        searchPatientsFail(error);
        showMessage({ message: error.message, type: "danger" });
      });
  };

  const onSearchInput = (searchValue: string) => {
    if (!searchValue || searchValue.length === 0) {
      setSelectedFacilityIndex(0);
      getPatients({
        startsWith: undefined,
        facility: FacilityConstants.FACILITIES_LIST[0],
        status: PatientStatusConstants.ACTIVE,
      });
    } else {
      setSelectedFacilityIndex(undefined);
      getPatients({
        startsWith: searchValue,
        facility: undefined,
        status: undefined,
      });
    }
  };

  const onPatientPress = (e: GestureResponderEvent, patient: IPatient) => {
    CredentialsService().updateRecentPatientSearches(patient);
    navigation.navigate("Patient", {
      patientId: patient.id,
    });
  };

  const onSelectedFacilityChange = (facilityIndex: number) => {
    setSelectedFacilityIndex(facilityIndex);

    getPatients({
      startsWith: undefined,
      facility: FacilityConstants.FACILITIES_LIST[facilityIndex],
      status: PatientStatusConstants.ACTIVE,
    });
  };

  const onSearchValueChange = (searchValue: string) => {
    setSearchTerm(searchValue);
    activateSearchLoader();
    const timeOutId = setTimeout(() => onSearchInput(searchValue), 150);
    return () => clearTimeout(timeOutId);
  };

  return (
    <AppLayout>
      <SearchBar
        containerStyle={searchContainer}
        inputContainerStyle={searchInputContainerStyle}
        inputStyle={{}}
        leftIconContainerStyle={searchIcon}
        rightIconContainerStyle={{}}
        loadingProps={{ animating: patientsLoading }}
        onChangeText={(newVal) => onSearchValueChange(newVal)}
        placeholder="Pretraga pacijenata..."
        cancelButtonProps={{ accessible: false }}
        value={searchTerm}
      />
      <Tab
        containerStyle={tabBar}
        value={selectedFacilityIndex}
        onChange={(selectedFacilityIndex) =>
          onSelectedFacilityChange(selectedFacilityIndex)
        }
        disableIndicator={true}
        scrollable={true}
      >
        {FacilityConstants.FACILITIES_LIST.map(
          (facility: string, index: number) => (
            <Tab.Item
              key={index}
              containerStyle={
                selectedFacilityIndex === index
                  ? { ...tab, ...selectedTab }
                  : tab
              }
              title={FacilityConstants.FACILITIES_NAMES[index]}
              titleStyle={{ fontSize: 15 }}
            />
          )
        )}
      </Tab>
      <Divider
        style={{ marginTop: 10, borderWidth: 1, borderColor: "#9bd3ae" }}
      />
      <PatientsList
        patientRooms={patientRooms}
        onPress={onPatientPress}
        loading={patientsLoading || recentPatientsLoading || false}
      />
    </AppLayout>
  );
};

const mapStateToProps = (store: AppState) => ({
  patientSearchState: store.patientSearch,
});

const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      searchPatients: searchPatients,
      searchPatientsSuccess: searchPatientsSuccess,
      searchPatientsFail: searchPatientsFail,
      getRecentPatients: getRecentPatients,
      getRecentPatientsSuccess: getRecentPatientsSuccess,
      getRecentPatientsFail: getRecentPatientsFail,
      activateSearchLoader: activateSearchLoader,
      deactivateSearchLoader: deactivateSearchLoader,
      resetPatients: resetPatients,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PatientSearchScreen);
