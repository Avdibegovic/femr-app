import React from "react";
import { GestureResponderEvent, View } from "react-native";
import { ScrollView, TouchableOpacity } from "react-native-gesture-handler";
import { PatientSearchScreenStyle } from "../patient.search.screen.style";
import { ListItem, Tab } from "@rneui/themed";
import { IPatient } from "../../../model/Patient";
import ProfileAvatar from "../../../../base/components/avatar/profileAvatar.component";
import Loader from "../../../../base/components/loader/loader.component";
import SearchMessage from "./searchMessage.component";
import { IPatientRoom } from "../../../model/PatientRoom";
import { Layout, Text } from "@ui-kitten/components";

export interface Props {
  patientRooms?: IPatientRoom[];
  onPress: (e: GestureResponderEvent, patient: IPatient) => void;
  loading: boolean;
}

const { patientlistItemContainerStyle, patientListStyle, roomNumberSytle } =
  PatientSearchScreenStyle;

const PatientsList = (props: Props) => {
  const { patientRooms, onPress, loading } = props;

  let sortedPatientRooms = patientRooms
    ?.slice()
    .sort((a: IPatientRoom, b: IPatientRoom) => {
      const roomNumberA = Number(a.roomNumber);
      const roomNumberB = Number(b.roomNumber);

      if (!roomNumberA && roomNumberB) {
        return 1;
      }
      if (roomNumberA && !roomNumberB) {
        return -1;
      }
      if (!roomNumberA && !roomNumberB) {
        return 0;
      }
      return roomNumberA - roomNumberB;
    });

  const patientsListItems: JSX.Element[] = [];

  if (loading) {
    return <Loader />;
  }

  if (sortedPatientRooms?.length === 0) {
    return <SearchMessage text={"Nema podataka"} />;
  }

  (sortedPatientRooms || []).forEach(
    (patientRoom: IPatientRoom, roomIndex: number) => {
      patientsListItems.push(
        <View
          key={`room-${roomIndex}`}
          style={{
            alignItems: "center",
          }}
        ></View>
      );
      patientRoom.patients.forEach(
        (patient: IPatient, patientIndex: number) => {
          patientsListItems.push(
            <ListItem
              key={`room-${roomIndex}-patient-${patientIndex}`}
              Component={TouchableOpacity}
              containerStyle={patientlistItemContainerStyle}
              disabledStyle={{ opacity: 0.5 }}
              onPress={(e) => onPress(e, patient)}
              pad={20}
            >
              <ProfileAvatar
                firstname={patient.firstname}
                lastname={patient.lastname}
              />
              <ListItem.Content>
                <ListItem.Title>
                  <Text>{`${patient.firstname} ${patient.lastname}`}</Text>
                </ListItem.Title>
              </ListItem.Content>
              {(patientRoom.roomNumber || "").length !== 0 && (
                <ListItem.Content right>
                  <Text style={roomNumberSytle}>{patientRoom.roomNumber}</Text>
                </ListItem.Content>
              )}
            </ListItem>
          );
        }
      );
    }
  );

  return <ScrollView style={patientListStyle}>{patientsListItems}</ScrollView>;
};

export default PatientsList;
