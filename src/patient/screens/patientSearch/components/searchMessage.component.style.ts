import { StyleSheet } from "react-native";

export const SearchMessageStyle = StyleSheet.create({
  container: {
    backgroundColor: "white",
    marginTop: 20,
    marginBottom: 20,
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 15,
    borderWidth: 1,
    borderColor: "#4ea392",
    padding: 20,
    alignItems: "center",
  },
});
