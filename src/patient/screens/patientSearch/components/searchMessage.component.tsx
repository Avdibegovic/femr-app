import React from "react";
import { Text, View } from "react-native";
import { SearchMessageStyle } from "./searchMessage.component.style";

export interface Props {
  text: string;
}

const { container } = SearchMessageStyle;

const SearchMessage = (props: Props) => {
  const { text } = props;
  return (
    <View style={container}>
      <Text>{text}</Text>
    </View>
  );
};

export default SearchMessage;
