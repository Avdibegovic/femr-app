import React, { useEffect } from "react";
import { Pso2OverviewState } from "../../store/pso2Overview/Pso2OverviewState";
import * as ScreenOrientation from "expo-screen-orientation";
import PatientVitalsService from "../../service/PatientVitalsService";
import { IPso2OverviewItem } from "../../model/Pso2OverviewItem";
import { showMessage } from "react-native-flash-message";
import { BackHandler } from "react-native";
import AppLayout from "../../../base/components/layout/app.layout";
import Loader from "../../../base/components/loader/loader.component";
import Pso2Chart from "./components/pso2Chart.component";
import { AppState } from "../../../base/store/initial/AppState";
import { bindActionCreators } from "@reduxjs/toolkit";
import {
  getPso2Overview,
  getPso2OverviewFail,
  getPso2OverviewSuccess,
} from "../../store/pso2Overview/pso2Overview.actions";
import { connect } from "react-redux";

interface IPso2OverviewProps {
  navigation: any;
  route: any;

  pso2OverviewState: Pso2OverviewState;

  getPso2Overview: Function;
  getPso2OverviewSuccess: Function;
  getPso2OverviewFail: Function;
}

const Pso2OverviewScreen = (props: IPso2OverviewProps) => {
  const {
    navigation,
    route,
    pso2OverviewState,
    getPso2Overview,
    getPso2OverviewSuccess,
    getPso2OverviewFail,
  } = props;

  const { patientId } = route.params;

  useEffect(() => {
    getPso2Overview();
    PatientVitalsService()
      .getPso2Overview(patientId)
      .then((pso2Overview: IPso2OverviewItem[]) => {
        getPso2OverviewSuccess(pso2Overview);
      })
      .catch((error) => {
        getPso2OverviewFail(error);
        showMessage({ message: error.message, type: "danger" });
      });
  }, []);

  useEffect(() => {
    const backAction = () => {
      ScreenOrientation.lockAsync(ScreenOrientation.OrientationLock.PORTRAIT);
      navigation.goBack();
      return true;
    };

    BackHandler.addEventListener("hardwareBackPress", backAction);
  }, []);

  return (
    <AppLayout>
      {pso2OverviewState.pso2OverviewLoading ? (
        <Loader />
      ) : (
        <Pso2Chart
          pso2Overview={
            pso2OverviewState.pso2Overview || ([] as IPso2OverviewItem[])
          }
        />
      )}
    </AppLayout>
  );
};

const mapStateToProps = (store: AppState) => ({
  pso2OverviewState: store.pso2Overview,
});

const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      getPso2Overview: getPso2Overview,
      getPso2OverviewSuccess: getPso2OverviewSuccess,
      getPso2OverviewFail: getPso2OverviewFail,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(Pso2OverviewScreen);
