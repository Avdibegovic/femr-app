import React from "react";
import { Dimensions, View } from "react-native";
import { LineChart } from "react-native-chart-kit";
import DateUtils from "../../../../base/utils/DateUtils";
import { IPso2OverviewItem } from "../../../model/Pso2OverviewItem";
import { Text as TextSVG } from "react-native-svg";

interface Props {
  pso2Overview: IPso2OverviewItem[];
}

const Pso2Chart = (props: Props) => {
  const { pso2Overview } = props;

  const chartConfig = {
    backgroundColor: "whitesmoke",
    backgroundGradientFrom: "whitesmoke",
    backgroundGradientTo: "whitesmoke",
    decimalPlaces: 2,
    color: (opacity = 1) => `black`,
    labelColor: (opacity = 1) => `black`,
    style: {
      borderRadius: 16,
    },
    propsForDots: {
      r: "6",
      strokeWidth: "2",
      stroke: "#ffa726",
    },
  };

  const labels = pso2Overview.map((item: IPso2OverviewItem) => {
    return DateUtils().formatDateWithTime(item.date);
  });

  const pso2 = pso2Overview.map((item: IPso2OverviewItem) => {
    return item.pso2;
  });

  const data = {
    labels: labels.length !== 0 ? labels : ["Nema podataka"],
    datasets: [
      {
        data: pso2.length !== 0 ? pso2 : [0],
        color: (opacity = 1) => `blue`,
        strokeWidth: 2,
      },
    ],
    legend: ["Pso2"],
  };

  return (
    <View>
      <LineChart
        data={data}
        chartConfig={chartConfig}
        height={Dimensions.get("screen").height - 100}
        width={Dimensions.get("screen").width - 30}
        renderDotContent={({ x, y, index, indexData }) => {
          return (
            <TextSVG
              key={`${x} - ${y} - ${index}`}
              x={x}
              y={y - 10}
              fill="black"
              fontSize="16"
              fontWeight="normal"
              textAnchor="middle"
            >
              {indexData}
            </TextSVG>
          );
        }}
      />
    </View>
  );
};

export default Pso2Chart;
