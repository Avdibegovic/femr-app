import { bindActionCreators } from "@reduxjs/toolkit";
import React, { useEffect } from "react";
import { connect } from "react-redux";
import { AppState } from "../../../base/store/initial/AppState";
import {
  getPulseOverview,
  getPulseOverviewFail,
  getPulseOverviewSuccess,
} from "../../store/pulseOverview/pulseOverview.actions";
import { PulseOverviewState } from "../../store/pulseOverview/PulseOverviewState";
import * as ScreenOrientation from "expo-screen-orientation";
import PatientVitalsService from "../../service/PatientVitalsService";
import { IPulseOverviewItem } from "../../model/PulseOverviewItme";
import { showMessage } from "react-native-flash-message";
import { BackHandler } from "react-native";
import AppLayout from "../../../base/components/layout/app.layout";
import Loader from "../../../base/components/loader/loader.component";
import PulseChart from "./components/pulseChart.component";

interface IPulseOverviewProps {
  navigation: any;
  route: any;

  pulseOverviewState: PulseOverviewState;

  getPulseOverview: Function;
  getPulseOverviewSuccess: Function;
  getPulseOverviewFail: Function;
}

const PulseOverviewScreen = (props: IPulseOverviewProps) => {
  const {
    navigation,
    route,
    pulseOverviewState,
    getPulseOverview,
    getPulseOverviewSuccess,
    getPulseOverviewFail,
  } = props;

  const { patientId } = route.params;

  useEffect(() => {
    getPulseOverview();
    PatientVitalsService()
      .getPulseOverview(patientId)
      .then((pulseOverview: IPulseOverviewItem[]) => {
        getPulseOverviewSuccess(pulseOverview);
      })
      .catch((error) => {
        getPulseOverviewFail(error);
        showMessage({ message: error.message, type: "danger" });
      });
  }, []);

  useEffect(() => {
    const backAction = () => {
      ScreenOrientation.lockAsync(ScreenOrientation.OrientationLock.PORTRAIT);
      navigation.goBack();
      return true;
    };

    BackHandler.addEventListener("hardwareBackPress", backAction);
  }, []);

  return (
    <AppLayout>
      {pulseOverviewState.pulseOverviewLoading ? (
        <Loader />
      ) : (
        <PulseChart
          pulseOverview={
            pulseOverviewState.pulseOverview || ([] as IPulseOverviewItem[])
          }
        />
      )}
    </AppLayout>
  );
};

const mapStateToProps = (store: AppState) => ({
  pulseOverviewState: store.pulseOverview,
});

const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      getPulseOverview: getPulseOverview,
      getPulseOverviewSuccess: getPulseOverviewSuccess,
      getPulseOverviewFail: getPulseOverviewFail,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PulseOverviewScreen);
