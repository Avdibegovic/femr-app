import React from "react";
import { Dimensions, Text, View } from "react-native";
import { LineChart } from "react-native-chart-kit";
import DateUtils from "../../../../base/utils/DateUtils";
import { IPulseOverviewItem } from "../../../model/PulseOverviewItme";
import { Text as TextSVG } from "react-native-svg";

interface Props {
  pulseOverview: IPulseOverviewItem[];
}

const PulseChart = (props: Props) => {
  const { pulseOverview } = props;

  const chartConfig = {
    backgroundColor: "whitesmoke",
    backgroundGradientFrom: "whitesmoke",
    backgroundGradientTo: "whitesmoke",
    color: (opacity = 1) => `black`,
    labelColor: (opacity = 1) => `black`,
    style: {
      borderRadius: 16,
    },
    propsForDots: {
      r: "6",
      strokeWidth: "2",
      stroke: "#ffa726",
    },
  };

  const labels = pulseOverview.map((item: IPulseOverviewItem) => {
    return DateUtils().formatDateWithTime(item.date);
  });

  const pulse = pulseOverview.map((item: IPulseOverviewItem) => {
    return item.pulse;
  });

  const data = {
    labels: labels.length !== 0 ? labels : ["Nema podataka"],
    datasets: [
      {
        data: pulse.length !== 0 ? pulse : [0],
        color: (opacity = 1) => `blue`,
        strokeWidth: 2,
      },
    ],
    legend: ["puls"],
  };

  return (
    <View>
      <LineChart
        data={data}
        chartConfig={chartConfig}
        height={Dimensions.get("screen").height - 100}
        width={Dimensions.get("screen").width - 30}
        renderDotContent={({ x, y, index, indexData }) => {
          return (
            <TextSVG
              key={`${x} - ${y} - ${index}`}
              x={x}
              y={y - 10}
              fill="black"
              fontSize="16"
              fontWeight="normal"
              textAnchor="middle"
            >
              {indexData}
            </TextSVG>
          );
        }}
      />
    </View>
  );
};

export default PulseChart;
