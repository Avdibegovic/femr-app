import { StyleSheet } from "react-native";

export const VitalsInputScreenStyle = StyleSheet.create({
  form: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    paddingBottom: 30,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
  },
  inputField: {
    borderWidth: 0,
  },
  icon: {
    color: "#ed1c24",
    marginLeft: 12,
  },
  formTitle: {
    color: "#6ca59a",
    fontSize: 30,
    marginVertical: 10,
    fontWeight: "bold",
  },
  submitButtonStyle: {
    backgroundColor: "#ed1c24",
    borderRadius: 10,
  },
  submitButtonTitleStyle: {
    fontWeight: "bold",
    fontSize: 23,
  },
  submitButtonContainerStyle: {
    marginHorizontal: 50,
    height: 50,
    width: "75%",
    marginVertical: 10,
  },
});
