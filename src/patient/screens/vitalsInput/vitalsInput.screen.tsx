import { bindActionCreators } from "@reduxjs/toolkit";
import { Button } from "@rneui/base";
import { Icon, Input } from "@rneui/themed";
import { Formik } from "formik";
import React, { useState } from "react";
import { ScrollView, Text, View } from "react-native";
import { showMessage } from "react-native-flash-message";
import { connect } from "react-redux";
import AppLayout from "../../../base/components/layout/app.layout";
import { AppState } from "../../../base/store/initial/AppState";
import TemperatureUtils from "../../../base/utils/TemperatureUtils";
import PatientVitalsMeasurement, {
  IPatientVitalsMeasurement,
} from "../../model/PatientVitalsMeasurement";
import PatientVitalsService from "../../service/PatientVitalsService";
import { getLatestPatientVitals } from "../../store/patientVitals/patientVitals.actions";
import {
  insertVitals,
  insertVitalsFail,
  insertVitalsSuccess,
} from "../../store/vitalsInput/vitalsInput.actions";
import { VitalsInputState } from "../../store/vitalsInput/VitalsInputState";
import { vitalsInputForm as vitalsInputSchema } from "./vitalsInput.form";
import { VitalsInputScreenStyle } from "./vitalsInput.screen.style";

interface VitalsInputScreenProps {
  navigation: any;
  route: any;

  vitalsInputState: VitalsInputState;
  insertVitals: Function;
  insertVitalsSuccess: Function;
  insertVitalsFail: Function;
  getLatestPatientVitals: Function;
}

const VitalsInputScreen = (props: VitalsInputScreenProps) => {
  const {
    navigation,
    insertVitals,
    insertVitalsSuccess,
    insertVitalsFail,
    route,
    getLatestPatientVitals,
  } = props;

  const { patientId } = route.params;

  const [patientVitalsMeasurement, setPatientVitalsMeasurement] = useState(
    PatientVitalsMeasurement({} as any)
  );

  const validateVitalsInput = (vitals: IPatientVitalsMeasurement) => {
    const {
      bpd,
      bps,
      temperature,
      pso2,
      pulse,
      hydration,
      bloodGlucose,
      diuresis,
    } = vitals;

    if (
      bpd ||
      bps ||
      temperature ||
      pso2 ||
      pulse ||
      hydration ||
      bloodGlucose ||
      diuresis
    ) {
      return;
    }
    throw new Error("Upis bar jednog vitalnog parametra je obavezan");
  };

  const onSubmit = (vitals: IPatientVitalsMeasurement) => {
    if (vitals.temperature) {
      vitals.temperature = TemperatureUtils().convertToFahrenheit(
        vitals.temperature
      );
    }
    if (vitals.bloodGlucose) {
      vitals.bloodGlucose = Number(
        (vitals.bloodGlucose as any).replace(",", ".")
      );
    }
    vitals.date = new Date();
    setPatientVitalsMeasurement(vitals);
    insertVitals();

    try {
      validateVitalsInput(vitals);

      PatientVitalsService()
        .insertPatientVitals(patientId, vitals)
        .then(() => {
          insertVitalsSuccess();
          getLatestPatientVitals();
          navigation.goBack();
          showMessage({
            message: "Vitalni parametri uspješno upisani",
            type: "success",
          });
        })
        .catch((error: any) => {
          throw error;
        });
    } catch (error: any) {
      insertVitalsFail(error);
      showMessage({ message: error.message, type: "danger" });
    }
  };

  return (
    <AppLayout>
      <ScrollView>
        <View style={VitalsInputScreenStyle.form}>
          <Text style={VitalsInputScreenStyle.formTitle}>
            Unos vitalnih parametara
          </Text>
          <Formik
            initialValues={PatientVitalsMeasurement({} as any)}
            onSubmit={onSubmit}
            validationSchema={vitalsInputSchema}
          >
            {({
              handleSubmit,
              handleChange,
              errors,
              touched,
              setFieldTouched,
            }) => (
              <>
                <Input
                  leftIcon={
                    <Icon
                      name="vertical-align-top"
                      type="material"
                      size={25}
                      style={{
                        marginLeft: VitalsInputScreenStyle.icon.marginLeft,
                      }}
                      color={VitalsInputScreenStyle.icon.color}
                    />
                  }
                  placeholder="BPS"
                  autoCapitalize="none"
                  autoCorrect={false}
                  keyboardType="number-pad"
                  returnKeyType="next"
                  onChangeText={handleChange("bps")}
                  onFocus={() => setFieldTouched("bps")}
                  maxLength={3}
                />
                <Input
                  leftIcon={
                    <Icon
                      name="vertical-align-bottom"
                      type="material"
                      style={{
                        marginLeft: VitalsInputScreenStyle.icon.marginLeft,
                      }}
                      color={VitalsInputScreenStyle.icon.color}
                      size={25}
                    />
                  }
                  placeholder="BPD"
                  autoCapitalize="none"
                  autoCorrect={false}
                  keyboardType="numeric"
                  returnKeyType="next"
                  onChangeText={handleChange("bpd")}
                  onFocus={() => setFieldTouched("bpd")}
                />
                <Input
                  leftIcon={
                    <Icon
                      name="favorite-border"
                      type="material"
                      size={25}
                      style={{
                        marginLeft: VitalsInputScreenStyle.icon.marginLeft,
                      }}
                      color={VitalsInputScreenStyle.icon.color}
                    />
                  }
                  placeholder="Puls"
                  autoCapitalize="none"
                  autoCorrect={false}
                  keyboardType="numeric"
                  returnKeyType="next"
                  onChangeText={handleChange("pulse")}
                  onFocus={() => setFieldTouched("pulse")}
                />
                <Input
                  leftIcon={
                    <Icon
                      name="device-thermostat"
                      type="material"
                      size={25}
                      style={{
                        marginLeft: VitalsInputScreenStyle.icon.marginLeft,
                      }}
                      color={VitalsInputScreenStyle.icon.color}
                    />
                  }
                  placeholder="Temperatura"
                  autoCapitalize="none"
                  autoCorrect={false}
                  keyboardType="decimal-pad"
                  returnKeyType="next"
                  onChangeText={handleChange("temperature")}
                  onFocus={() => setFieldTouched("temperature")}
                />
                <Input
                  leftIcon={
                    <Icon
                      name="wind"
                      type="feather"
                      size={25}
                      style={{
                        marginLeft: VitalsInputScreenStyle.icon.marginLeft,
                      }}
                      color={VitalsInputScreenStyle.icon.color}
                    />
                  }
                  placeholder="SPO2"
                  autoCapitalize="none"
                  autoCorrect={false}
                  keyboardType="numeric"
                  returnKeyType="next"
                  onChangeText={handleChange("pso2")}
                  onFocus={() => setFieldTouched("pso2")}
                />
                <Input
                  leftIcon={
                    <Icon
                      name="drop"
                      type="simple-line-icon"
                      size={25}
                      style={{
                        marginLeft: VitalsInputScreenStyle.icon.marginLeft,
                      }}
                      color={VitalsInputScreenStyle.icon.color}
                    />
                  }
                  placeholder="Hidracija (ml)"
                  autoCapitalize="none"
                  autoCorrect={false}
                  keyboardType="numeric"
                  returnKeyType="next"
                  onChangeText={handleChange("hydration")}
                  onFocus={() => setFieldTouched("hydration")}
                />
                <Input
                  leftIcon={
                    <Icon
                      name="analytics"
                      type="material"
                      size={25}
                      style={{
                        marginLeft: VitalsInputScreenStyle.icon.marginLeft,
                      }}
                      color={VitalsInputScreenStyle.icon.color}
                    />
                  }
                  placeholder="Diureza"
                  autoCapitalize="none"
                  autoCorrect={false}
                  keyboardType="numeric"
                  returnKeyType="next"
                  onChangeText={handleChange("diuresis")}
                  onFocus={() => setFieldTouched("diuresis")}
                />
                <Input
                  leftIcon={
                    <Icon
                      name="analytics"
                      type="material"
                      size={25}
                      style={{
                        marginLeft: VitalsInputScreenStyle.icon.marginLeft,
                      }}
                      color={VitalsInputScreenStyle.icon.color}
                    />
                  }
                  placeholder="Šuk"
                  autoCapitalize="none"
                  autoCorrect={false}
                  keyboardType="numeric"
                  returnKeyType="next"
                  onChangeText={handleChange("bloodGlucose")}
                  onFocus={() => setFieldTouched("bloodGlucose")}
                />
                <Button
                  title="Spasi"
                  loading={false}
                  loadingProps={{ size: "small", color: "white" }}
                  buttonStyle={VitalsInputScreenStyle.submitButtonStyle}
                  titleStyle={VitalsInputScreenStyle.submitButtonTitleStyle}
                  containerStyle={
                    VitalsInputScreenStyle.submitButtonContainerStyle
                  }
                  onPress={handleSubmit as any}
                />
              </>
            )}
          </Formik>
        </View>
      </ScrollView>
    </AppLayout>
  );
};

const mapStateToProps = (store: AppState) => ({
  vitalsInputState: store.vitalsInput,
});

const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      insertVitals: insertVitals,
      insertVitalsSuccess: insertVitalsSuccess,
      insertVitalsFail: insertVitalsFail,
      getLatestPatientVitals: getLatestPatientVitals,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(VitalsInputScreen);
