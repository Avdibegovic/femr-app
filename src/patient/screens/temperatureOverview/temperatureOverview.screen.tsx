import React, { useEffect } from "react";
import { TemperatureOverviewState } from "../../store/temperatureOverview/TemperatureOverviewState";
import * as ScreenOrientation from "expo-screen-orientation";
import PatientVitalsService from "../../service/PatientVitalsService";
import { ITemperatureOverviewItem } from "../../model/TemperatureOverviewItem";
import { showMessage } from "react-native-flash-message";
import { BackHandler } from "react-native";
import AppLayout from "../../../base/components/layout/app.layout";
import Loader from "../../../base/components/loader/loader.component";
import TemperatureChart from "./components/temperatureChart.component";
import { AppState } from "../../../base/store/initial/AppState";
import { bindActionCreators } from "@reduxjs/toolkit";
import {
  getTemperatureOverview,
  getTemperatureOverviewFail,
  getTemperatureOverviewSuccess,
} from "../../store/temperatureOverview/temperatureOverview.actions";
import { connect } from "react-redux";

interface ITemperatuteOverviewProps {
  navigation: any;
  route: any;

  temperatureOverviewState: TemperatureOverviewState;

  getTemperatureOverview: Function;
  getTemperatureOverviewSuccess: Function;
  getTemperatureOverviewFail: Function;
}

const TemperatureOverviewScreen = (props: ITemperatuteOverviewProps) => {
  const {
    navigation,
    route,
    temperatureOverviewState,
    getTemperatureOverview,
    getTemperatureOverviewSuccess,
    getTemperatureOverviewFail,
  } = props;

  const { patientId } = route.params;

  useEffect(() => {
    getTemperatureOverview();
    PatientVitalsService()
      .getTemperatureOverview(patientId)
      .then((temperatureOverview: ITemperatureOverviewItem[]) => {
        getTemperatureOverviewSuccess(temperatureOverview);
      })
      .catch((error) => {
        getTemperatureOverviewFail(error);
        showMessage({ message: error.message, type: "danger" });
      });
  }, []);

  useEffect(() => {
    const backAction = () => {
      ScreenOrientation.lockAsync(ScreenOrientation.OrientationLock.PORTRAIT);
      navigation.goBack();
      return true;
    };

    BackHandler.addEventListener("hardwareBackPress", backAction);
  }, []);

  return (
    <AppLayout>
      {temperatureOverviewState.temperatureOverviewLoading ? (
        <Loader />
      ) : (
        <TemperatureChart
          temperatureOverview={
            temperatureOverviewState.temperatureOverview ||
            ([] as ITemperatureOverviewItem[])
          }
        />
      )}
    </AppLayout>
  );
};

const mapStateToProps = (store: AppState) => ({
  temperatureOverviewState: store.temperatureOverview,
});

const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      getTemperatureOverview: getTemperatureOverview,
      getTemperatureOverviewSuccess: getTemperatureOverviewSuccess,
      getTemperatureOverviewFail: getTemperatureOverviewFail,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TemperatureOverviewScreen);
