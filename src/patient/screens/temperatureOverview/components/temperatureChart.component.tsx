import React from "react";
import { Dimensions, View } from "react-native";
import { LineChart } from "react-native-chart-kit";
import DateUtils from "../../../../base/utils/DateUtils";
import TemperatureUtils from "../../../../base/utils/TemperatureUtils";
import { ITemperatureOverviewItem } from "../../../model/TemperatureOverviewItem";
import { Text as TextSVG } from "react-native-svg";

interface Props {
  temperatureOverview: ITemperatureOverviewItem[];
}

const TemperatureChart = (props: Props) => {
  const { temperatureOverview } = props;

  const chartConfig = {
    backgroundColor: "whitesmoke",
    backgroundGradientFrom: "whitesmoke",
    backgroundGradientTo: "whitesmoke",
    decimalPlaces: 2,
    color: (opacity = 1) => `black`,
    labelColor: (opacity = 1) => `black`,
    style: {
      borderRadius: 16,
    },
    propsForDots: {
      r: "6",
      strokeWidth: "2",
      stroke: "#ffa726",
    },
  };

  const labels = temperatureOverview.map((item: ITemperatureOverviewItem) => {
    return DateUtils().formatDateWithTime(item.date);
  });

  const temperatures = temperatureOverview.map(
    (item: ITemperatureOverviewItem) => {
      return TemperatureUtils().convertToCelsius(item.temperature);
    }
  );

  const data = {
    labels: labels.length !== 0 ? labels : ["Nema podataka"],
    datasets: [
      {
        data: temperatures.length !== 0 ? temperatures : [0],
        color: (opacity = 1) => `blue`,
        strokeWidth: 2,
      },
    ],
    legend: ["Temperatura"],
  };

  return (
    <View>
      <LineChart
        data={data}
        chartConfig={chartConfig}
        height={Dimensions.get("screen").height - 100}
        width={Dimensions.get("screen").width - 30}
        renderDotContent={({ x, y, index, indexData }) => {
          return (
            <TextSVG
              key={`${x} - ${y} - ${index}`}
              x={x}
              y={y - 10}
              fill="black"
              fontSize="16"
              fontWeight="normal"
              textAnchor="middle"
            >
              {indexData}
            </TextSVG>
          );
        }}
      />
    </View>
  );
};

export default TemperatureChart;
