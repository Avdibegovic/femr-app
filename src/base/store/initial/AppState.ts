import { LoginState } from "../../../auth/store/LoginState";
import { BloodPressureOverviewState } from "../../../patient/store/bloodPressureOverview/BloodPressureOverviewState";
import { PatientState } from "../../../patient/store/patient/PatientState";
import { PatientLabState } from "../../../patient/store/patientLab/PatientLabState";
import { PatientPrescriptionsState } from "../../../patient/store/patientPrescriptions/PatientPrescriptionsState";
import { PatientSearchState } from "../../../patient/store/patientSearch/PatientSearchState";
import { PatientVitalsState } from "../../../patient/store/patientVitals/PatientVitalsState";
import { Pso2OverviewState } from "../../../patient/store/pso2Overview/Pso2OverviewState";
import { PulseOverviewState } from "../../../patient/store/pulseOverview/PulseOverviewState";
import { TemperatureOverviewState } from "../../../patient/store/temperatureOverview/TemperatureOverviewState";
import { VitalsInputState } from "../../../patient/store/vitalsInput/VitalsInputState";
import { LoadingState } from "../loading/loadingState";

export interface AppState {
  loading: LoadingState;
  login: LoginState;
  patientSearch: PatientSearchState;
  patient: PatientState;
  patientVitals: PatientVitalsState;
  bloodPressureOverview: BloodPressureOverviewState;
  pulseOverview: PulseOverviewState;
  temperatureOverview: TemperatureOverviewState;
  pso2Overview: Pso2OverviewState;
  vitalsInput: VitalsInputState;
  patientLab: PatientLabState;
  patientPrescriptions: PatientPrescriptionsState;
}
