import { IBloodPressureOverviewItem } from "../../../patient/model/BloodPressureOverviewItem";
import { IPatient } from "../../../patient/model/Patient";
import { IPatientPrescription } from "../../../patient/model/PatientPrescription";
import { IPatientVitals } from "../../../patient/model/PatientVitals";
import { IPso2OverviewItem } from "../../../patient/model/Pso2OverviewItem";
import { IPulseOverviewItem } from "../../../patient/model/PulseOverviewItme";
import { ITemperatureOverviewItem } from "../../../patient/model/TemperatureOverviewItem";
import { AppState } from "./AppState";

export const AppInitialState: AppState = {
  loading: {
    show: false,
  },
  login: {
    error: null,
    isLoggedIn: false,
    isLoggingIn: false,
  },
  patientSearch: {
    error: null,
    isSearching: false,
    patientRooms: undefined,
    patientsLoading: false,
    recentPatientsLoading: false,
    recentPatients: undefined,
  },
  patient: {
    patient: {} as IPatient,
    patientLoading: true,
  },
  patientVitals: {
    latestPatientVitals: {} as IPatientVitals,
    patientVitalsLoading: true,
  },
  bloodPressureOverview: {
    bloodPressureOverview: [] as IBloodPressureOverviewItem[],
    bloodPressureOverviewLoading: true,
  },
  pulseOverview: {
    pulseOverview: [] as IPulseOverviewItem[],
    pulseOverviewLoading: true,
  },
  temperatureOverview: {
    temperatureOverview: [] as ITemperatureOverviewItem[],
    temperatureOverviewLoading: true,
  },
  pso2Overview: {
    pso2Overview: [] as IPso2OverviewItem[],
    pso2OverviewLoading: true,
  },
  vitalsInput: {
    isLoading: false,
  },
  patientLab: {
    labLoading: true,
    resultCodes: [] as string[],
    labMeasurements: undefined,
    labDates: [] as number[],
  },
  patientPrescriptions: {
    patientPrescriptions: [] as IPatientPrescription[],
    patientPrescriptionsLoading: true,
  },
};
