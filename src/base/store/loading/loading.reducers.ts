import { createReducer } from "@reduxjs/toolkit";
import { AppInitialState } from "../initial/AppInitialState";
import { hide, show } from "./loading.actions";
import { LoadingState } from "./loadingState";

const initialState: LoadingState = AppInitialState.loading;

export const loadingReducer = createReducer(initialState, (builder) => {
  builder.addCase(show, (currentState) => {
    return { ...currentState, show: true };
  });
  builder.addCase(hide, (currentState) => {
    return { ...currentState, show: false };
  });
});
