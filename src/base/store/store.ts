import { configureStore } from "@reduxjs/toolkit";
import { loginReducer } from "../../auth/store/login.reducer";
import { bloodPressureOverviewReducer } from "../../patient/store/bloodPressureOverview/bloodPressureOverview.reducer";
import { patientReducer } from "../../patient/store/patient/patient.reducer";
import { patientLabRedcuer } from "../../patient/store/patientLab/patientLab.reducer";
import { patientPrescriptionsReducer } from "../../patient/store/patientPrescriptions/patientPrescriptions.redcuer";
import { patientSearchReducer } from "../../patient/store/patientSearch/patientSearch.reducer";
import { patientVitalsReducer } from "../../patient/store/patientVitals/patientVitals.reducer";
import { pso2OverviewReducer } from "../../patient/store/pso2Overview/pso2Overview.reducer";
import { pulseOverviewReducer } from "../../patient/store/pulseOverview/pulseOverview.reducer";
import { temperatureOverviewReducer } from "../../patient/store/temperatureOverview/temperatureOverview.reducer";
import { vitalsInputReducer } from "../../patient/store/vitalsInput/vitalsInput.reducer";
import { loadingReducer } from "./loading/loading.reducers";

export const reducers = {
  loading: loadingReducer,
  login: loginReducer,
  patientSearch: patientSearchReducer,
  patient: patientReducer,
  patientVitals: patientVitalsReducer,
  bloodPressureOverview: bloodPressureOverviewReducer,
  pulseOverview: pulseOverviewReducer,
  temperatureOverview: temperatureOverviewReducer,
  pso2Overview: pso2OverviewReducer,
  vitalsInput: vitalsInputReducer,
  patientLab: patientLabRedcuer,
  patientPrescriptions: patientPrescriptionsReducer,
};

export const store = configureStore({
  reducer: reducers,
});
