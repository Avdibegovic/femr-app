export interface TNetworkError {
  name: string;
  message: string;
  code: string;
}

const getErrorMessage = (msg?: string) => {
  switch (msg) {
    case "Bad credentials":
      return "Neispravni korisniči podaci";
    case null || undefined:
      return "Došlo je do greške. Pokušajte ponovo ili kontaktirajte administratora.";
    default:
      return msg;
  }
};

const NetworkError = (error: TNetworkError) => {
  function Error(this: TNetworkError, { message, code }: TNetworkError) {
    this.name = "Error";
    this.message = getErrorMessage(message);
    this.code = code || "CLIENT_ERROR";
  }
  NetworkError.prototype = Object.create(Error.prototype);
  NetworkError.prototype.constructor = NetworkError;

  return new (Error as any)(error);
};

export default NetworkError;
