export interface IDatautils {
  decodeSex(sex: string): string;
  decodeDepartment(department: string): string;
}

const DataUtils = () => {
  return {
    decodeSex(sex: string) {
      switch (sex) {
        case "Male":
          return "Muško";
        case "Female":
          return "Žensko";
        default:
          return "";
      }
    },
    decodeDepartment(department: string) {
      switch (department) {
        case "none":
          return "";
        default:
          return department;
      }
    },
  };
};

export default DataUtils;
