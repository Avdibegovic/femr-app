import moment from "moment";
import { DateConstants } from "../constants/Constants";

export interface IDateUtils {
  formatDate(date: Date): string;
  formatDateWithTime(date: Date): string;
}

const DateUtils = (): IDateUtils => {
  return {
    formatDate(date: Date) {
      return moment(date).format(DateConstants.DATE_FORMAT);
    },
    formatDateWithTime(date: Date) {
      return moment(date).format(DateConstants.DATE_FORMAT_WITH_HOURS);
    },
  };
};

export default DateUtils;
