export interface ITemperatureUtils {
  convertToCelsius(temperature: number): number;
  convertToFahrenheit(temperature: number): number;
}

const TemperatureUtils = (): ITemperatureUtils => {
  return {
    convertToCelsius(temperature: number) {
      return Number(((5 / 9) * (temperature - 32)).toFixed(1));
    },
    convertToFahrenheit(temperature: number) {
      return (9 / 5) * temperature + 32;
    },
  };
};

export default TemperatureUtils;
