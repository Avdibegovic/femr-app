import React from "react";
import { Avatar } from "@rneui/themed";

export interface Props {
  firstname: string;
  lastname: string;
}

const COLOR_CODES = [
  "#34568B",
  "#FF6F61",
  "#6B5B95",
  "#88B04B",
  "#955251",
  "#B565A7",
  "#009B77",
  "#DD4124",
  "#D65076",
  "#45B8AC",
  "#EFC050",
  "#5B5EA6",
  "#9B2335",
  "#55B4B0",
  "#E15D44",
  "#7FCDCD",
  "#BC243C",
  "#C3447A",
];

const getAvatarTitle = (firstname: string, lastname: string) => {
  let firstInital =
    firstname[1] === "j" && (firstname[0] === "L" || firstname[0] === "N")
      ? firstname.slice(0, 2)
      : firstname[0];
  let secondInitial =
    lastname[1] === "j" && (firstname[0] === "L" || firstname[0] === "N")
      ? lastname.slice(0, 2)
      : lastname[0];

  return `${firstInital}${secondInitial}`;
};

const ProfileAvatar = (props: Props) => {
  const { firstname = "", lastname = "" } = props;
  const getAvatarColor = () => {
    const firstLetterASCIICode = firstname.charCodeAt(0);
    const secondLetterASCIICode = lastname.charCodeAt(0);

    const color =
      COLOR_CODES[
        (firstLetterASCIICode + secondLetterASCIICode) % COLOR_CODES.length
      ];

    return color;
  };

  return (
    <Avatar
      containerStyle={{ backgroundColor: getAvatarColor() }}
      rounded
      size="medium"
      title={getAvatarTitle(firstname, lastname)}
    />
  );
};

export default ProfileAvatar;
