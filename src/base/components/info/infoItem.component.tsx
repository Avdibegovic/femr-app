import { Layout, LayoutProps, Text } from "@ui-kitten/components";
import React from "react";
import { InfoItemStyle } from "./infoItem.component.style";

export interface IInfoItem extends LayoutProps {
  label: string;
  value: string;
  style?: any;
}

const { container } = InfoItemStyle;

const InfroItem = (props: IInfoItem) => {
  const { style, label, value, ...layoutProps } = props;
  return (
    <React.Fragment>
      <Layout {...layoutProps} level={"1"} style={{ ...container, ...style }}>
        <Text appearance={"hint"} category="h6">
          {label}
        </Text>
        <Text category="h6">{value}</Text>
      </Layout>
    </React.Fragment>
  );
};

export default InfroItem;
