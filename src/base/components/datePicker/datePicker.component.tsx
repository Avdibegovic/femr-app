import React, { useEffect } from "react";
import { Button } from "@rneui/base";
import { Calendar, DateData } from "react-native-calendars";
import moment from "moment";
import { Modal, View } from "react-native";
import DateUtils from "../../utils/DateUtils";
import { datePickerStyle } from "./datePicker.component.style";

const INPUT_DATE_FORMAT = "YYYY-MM-DD";

const MARKED_DOT = {
  key: "marked",
  color: "blue",
  selectedDotColor: "blue",
};

interface Props {
  defaultDate?: Date;
  startDate?: Date;
  endDate?: Date;
  markedDates?: Date[];
  onSelect?: (selectedDate: Date) => void;
}

const { centeredView, modalView, calendarStyle, openCalendarButtonStyle } =
  datePickerStyle;

const DatePicker = (props: Props) => {
  const { defaultDate, startDate, endDate, markedDates, onSelect } = props;

  const [selectedDate, setSelectedDate] = React.useState(
    (defaultDate || endDate || new Date()).getTime()
  );
  const [modalVisible, setModalVisible] = React.useState(false);

  const dateToInputString = (date: Date) => {
    return moment(date).format(INPUT_DATE_FORMAT);
  };

  const getMarkedDates = () => {
    let calendarMarkedDates = {};

    const selectedDateInputString = dateToInputString(new Date(selectedDate));
    calendarMarkedDates = {
      ...calendarMarkedDates,
      [selectedDateInputString]: {
        dots: [],
        selected: true,
        selectedColor: "green",
      },
    };

    markedDates?.forEach((markedDate: Date) => {
      const dateInputString = dateToInputString(markedDate);

      const isSelectedDate = markedDate.getTime() === selectedDate;
      calendarMarkedDates = {
        ...calendarMarkedDates,
        [dateInputString]: isSelectedDate
          ? {
              dots: [MARKED_DOT],
              selected: true,
              selectedColor: "green",
            }
          : { dots: [MARKED_DOT] },
      };
    });
    return calendarMarkedDates;
  };

  return (
    <>
      <Button
        buttonStyle={openCalendarButtonStyle}
        titleStyle={{ color: "black" }}
        onPress={() => setModalVisible(true)}
        type="outline"
        uppercase={false}
      >
        {DateUtils().formatDate(new Date(selectedDate))}
      </Button>
      <Modal transparent={true} animationType="slide" visible={modalVisible}>
        <View style={centeredView}>
          <View style={modalView}>
            <Calendar
              style={calendarStyle}
              initialDate={endDate ? dateToInputString(endDate) : undefined}
              minDate={startDate ? dateToInputString(startDate) : undefined}
              maxDate={endDate ? dateToInputString(endDate) : undefined}
              monthFormat={"MM yyyy"}
              hideExtraDays={true}
              disableMonthChange={true}
              firstDay={1}
              hideDayNames={true}
              showWeekNumbers={true}
              onPressArrowLeft={(subtractMonth) => subtractMonth()}
              onPressArrowRight={(addMonth) => addMonth()}
              disableAllTouchEventsForDisabledDays={true}
              enableSwipeMonths={true}
              markedDates={markedDates ? getMarkedDates() : undefined}
              markingType={"multi-dot"}
              onDayPress={async (date: DateData) => {
                setModalVisible(false);
                setSelectedDate(date.timestamp);
                onSelect?.(new Date(date.timestamp));
              }}
            />
            <Button
              type="outline"
              onPress={() => {
                setModalVisible(false);
              }}
            >
              Otkaži
            </Button>
          </View>
        </View>
      </Modal>
    </>
  );
};

export default DatePicker;
