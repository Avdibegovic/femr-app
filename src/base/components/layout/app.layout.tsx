import React from "react";
import { SafeAreaView } from "react-native";
import { appLayoutStyle } from "./app.layout.style";

export interface IAppLayout {
  children?: any;
}

const AppLayout = (props: IAppLayout) => {
  const { children } = props;
  return (
    <SafeAreaView style={appLayoutStyle.container}>{children}</SafeAreaView>
  );
};

export default AppLayout;
