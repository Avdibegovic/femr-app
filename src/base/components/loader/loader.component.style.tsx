import { StyleSheet } from "react-native";

export const LoaderStyle = StyleSheet.create({
  container: {
    marginTop: 20,
    alignItems: "center",
  },
});
