import { Spinner } from "@ui-kitten/components";
import React from "react";
import { View } from "react-native";
import { LoaderStyle } from "./loader.component.style";

const { container } = LoaderStyle;

const Loader = () => {
  return (
    <View style={container}>
      <Spinner status={"success"} size={"large"} />
    </View>
  );
};

export default Loader;
