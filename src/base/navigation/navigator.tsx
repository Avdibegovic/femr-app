import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import React from "react";
import loginScreen from "../../auth/screens/login/login.screen";
import BloodPressureOverviewScreen from "../../patient/screens/bloodPressureOverview/bloodPressureOverview.screen";
import PatientScreen from "../../patient/screens/patient/patient.screen";
import patientSearchScreen from "../../patient/screens/patientSearch/patient.search.screen";
import pso2OverviewScreen from "../../patient/screens/pso2Overview/pso2Overview.screen";
import pulseOverviewScreen from "../../patient/screens/pulseOverview/pulseOverview.screen";
import temperatureOverviewScreen from "../../patient/screens/temperatureOverview/temperatureOverview.screen";
import vitalsInputScreen from "../../patient/screens/vitalsInput/vitalsInput.screen";

const { Navigator, Screen } = createStackNavigator();

const AppNavigator = () => {
  const defaultScreenOptions = {
    headerShown: false,
  };
  return (
    <NavigationContainer>
      <Navigator initialRouteName="Login">
        <Screen
          name="Login"
          component={loginScreen}
          options={{ ...defaultScreenOptions }}
        />
        <Screen
          name="PatientSearch"
          component={patientSearchScreen}
          options={{ ...defaultScreenOptions }}
        />
        <Screen
          name="Patient"
          component={PatientScreen}
          options={{ ...defaultScreenOptions }}
        />
        <Screen
          name="BloodPressureOverview"
          component={BloodPressureOverviewScreen}
          options={{ ...defaultScreenOptions }}
        />
        <Screen
          name="PulseOverview"
          component={pulseOverviewScreen}
          options={{ ...defaultScreenOptions }}
        />
        <Screen
          name="TemperatureOverview"
          component={temperatureOverviewScreen}
          options={{ ...defaultScreenOptions }}
        />
        <Screen
          name="Pso2Overview"
          component={pso2OverviewScreen}
          options={{ ...defaultScreenOptions }}
        />
        <Screen
          name="VitalsInput"
          component={vitalsInputScreen}
          options={{ ...defaultScreenOptions }}
        />
      </Navigator>
    </NavigationContainer>
  );
};

export default AppNavigator;
