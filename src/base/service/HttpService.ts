import axios from "axios";
import NetworkError from "../error/NetworkError";

export interface IHttpService {
  get(url: string, params?: Object): Promise<any>;
  post(url: string, params?: Object): Promise<any>;
  put(url: string, params?: Object): Promise<any>;
  remove(url: string, params?: Object): Promise<any>;
}

const HttpService = (): IHttpService => {
  const _baseUrl: string = "http://192.168.1.118:8090";
  return {
    async get(url: string, params?: Object) {
      try {
        const response = await axios.get(`${_baseUrl}${url}`, { params });
        return response;
      } catch (error) {
        throw new (NetworkError as any)((error as any)?.response?.data);
      }
    },
    async post(url: string, params?: Object) {
      try {
        const response = await axios.post(`${_baseUrl}${url}`, params);
        return response;
      } catch (error) {
        throw new (NetworkError as any)((error as any)?.response?.data);
      }
    },
    async put(url: string, params?: Object) {
      try {
        const response = await axios.put(`${_baseUrl}${url}`, { params });
        return response;
      } catch (error) {
        throw new (NetworkError as any)((error as any)?.response?.data);
      }
    },
    async remove(url: string, params?: Object) {
      try {
        const response = await axios.put(`${_baseUrl}${url}`, { params });
        return response;
      } catch (error) {
        throw new (NetworkError as any)((error as any)?.response?.data);
      }
    },
  };
};

export default HttpService;
