import AsyncStorage from "@react-native-async-storage/async-storage";
import PatientListMapper from "../../patient/model/mappers/PatientListMapper";
import { IPatient } from "../../patient/model/Patient";
import { PatientSearchConstants } from "../constants/Constants";

const RECENT_PATIENT_SEARCHES_KEY = "recentPatientSearches";

export interface ICredentialsService {
  updateRecentPatientSearches(patient: IPatient): Promise<void>;
  getRecentPatientSearches(): Promise<IPatient[] | null>;
}

const CredentialsService = (): ICredentialsService => {
  return {
    async updateRecentPatientSearches(patient: IPatient) {
      let recentPatientSearches = await this.getRecentPatientSearches();

      if (recentPatientSearches) {
        recentPatientSearches = recentPatientSearches.filter(
          (recentPatient: IPatient) => recentPatient.id !== patient.id
        );

        if (
          recentPatientSearches.length <
          PatientSearchConstants.RECENT_PATIENTS_COUNT
        ) {
          recentPatientSearches = [patient].concat(recentPatientSearches);
        } else {
          recentPatientSearches = [patient].concat(
            recentPatientSearches?.pop() || ([] as IPatient[])
          );
        }
      } else {
        recentPatientSearches = [patient];
      }
      await AsyncStorage.setItem(
        RECENT_PATIENT_SEARCHES_KEY,
        JSON.stringify(recentPatientSearches)
      );
    },

    async getRecentPatientSearches() {
      const value = await AsyncStorage.getItem(RECENT_PATIENT_SEARCHES_KEY);
      if (!value) {
        return null;
      }
      return PatientListMapper(JSON.parse(value));
    },
  };
};

export default CredentialsService;
