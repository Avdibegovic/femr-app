const PatientSearchConstants = {
  RECENT_PATIENTS_COUNT: 10,
};

const DateConstants = {
  DATE_FORMAT: "DD.MM.YYYY",
  DATE_FORMAT_WITH_HOURS: "DD.MM.YYYY HH:mm",
};

const FacilityConstants = {
  INTENSIVE_CARE: "intenzivna",
  NURSING_CARE: "dom",
  FACILITIES_LIST: ["intenzivna", "dom"],
  FACILITIES_NAMES: ["Intenzivna", "Dom"],
};

const PatientStatusConstants = {
  ACTIVE: "da",
  INACTIVE: "ne",
};

export { PatientSearchConstants };
export { DateConstants };
export { FacilityConstants };
export { PatientStatusConstants };
