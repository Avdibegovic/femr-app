import { Provider } from "react-redux";
import { store } from "./src/base/store/store";
import { ThemeProvider } from "@rneui/themed";
import { Dimensions } from "react-native";
import AppNavigator from "./src/base/navigation/navigator";
import LoadingComponent from "./src/base/components/loading/loading.component";
import FlashMessage from "react-native-flash-message";
import { ApplicationProvider } from "@ui-kitten/components";
import * as eva from "@eva-design/eva";

const SCREEN_WIDTH = Dimensions.get("window").width;

function App() {
  return (
    <ApplicationProvider {...eva} theme={eva.light}>
      <ThemeProvider
        theme={{
          Input: {
            containerStyle: {
              width: SCREEN_WIDTH - 50,
            },
            inputContainerStyle: {
              borderRadius: 40,
              borderWidth: 1,
              borderColor: "#9bd3ae",
              height: 50,
              marginVertical: 10,
              backgroundColor: "white",
            },
            placeholderTextColor: "#9bd3ae",
            inputStyle: {
              marginLeft: 10,
              color: "black",
            },
            keyboardAppearance: "dark",
            blurOnSubmit: false,
          },
        }}
      >
        <Provider store={store}>
          <AppNavigator />
          <LoadingComponent />
        </Provider>
        <FlashMessage position={"top"} />
      </ThemeProvider>
    </ApplicationProvider>
  );
}

export default App;
